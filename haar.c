#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "libISEN/BmpLib.h"
#include "structure.h"
#include "matrice.h"
#include "haar.h"

// pp : moyenne des moyennes
// pm : erreur des moyennes
// mp : moyenne des erreurs
// mm : erreur des erreurs
void decoupeImageEn4(matGrise *m,matGrise **pp,matGrise **pm,matGrise **mp,matGrise **mm)
{
	if(m != NULL)
	{
		matGrise *demiP, *demiM;
		demiP = demiM = NULL;
		int largeurBase = m->nbcol;
		int hauteurBase = m->nblig;

		// Les demi matrice ont une largeur divisé par 2 et la même hauteur
		int largeurDemi = (int)(largeurBase/2);
		int hauteurDemi = (int)(hauteurBase/2);

		alloueMatriceGrise(hauteurBase,largeurDemi,&demiP);
		alloueMatriceGrise(hauteurBase,largeurDemi,&demiM);

		demiMatriceHorizontale(m,demiP,1);
		demiMatriceHorizontale(m,demiM,0);
		alloueMatriceGrise(hauteurDemi,largeurDemi,pp);
		alloueMatriceGrise(hauteurDemi,largeurDemi,pm);
		alloueMatriceGrise(hauteurDemi,largeurDemi,mp);
		alloueMatriceGrise(hauteurDemi,largeurDemi,mm);

		demiMatriceVerticale(demiP,*pp,1);
		demiMatriceVerticale(demiP,*pm,0);
		demiMatriceVerticale(demiM,*mp,1);
		demiMatriceVerticale(demiM,*mm,0);

		libereMatriceGrise(&demiP);
		libereMatriceGrise(&demiM);
	}
}
/*
    operation :
		--> 0 = -
		--> 1 = +
*/
void demiMatriceHorizontale(matGrise *Base, matGrise *res, int operation)
{
	for(int i = 0; i < res->nblig; i++)
	{
		for (int j = 0; j < res->nbcol; j++)
		{
			if(operation == 1)
			{
				res->g[i][j] = (short int)((Base->g[i][j*2] + Base->g[i][j*2+1])/2);
				// printf("[%d][%d] = %d\n",i,j,res->g[i][j] );
			}
			else
			{
				res->g[i][j] = valeurAbsolue((Base->g[i][j*2] - Base->g[i][j*2+1])/2);

			}
		}
	}
}

void demiMatriceVerticale(matGrise *Base, matGrise *res, int operation)
{
	for(int i = 0; i < res->nblig; i++)
	{
		for (int j = 0; j < res->nbcol; j++)
		{
			if(operation == 1)
			{
				res->g[i][j] = (short int)((Base->g[i*2][j] + Base->g[i*2+1][j])/2);
			}
			else
			{
				res->g[i][j] = valeurAbsolue((Base->g[i*2][j] - Base->g[i*2+1][j])/2);

			}
		}
	}
}

short int valeurAbsolue(short int v)
{
	if(v <= 0)
		return -v;
	else
		return v;
}

void decomposeImage(matGrise *m, noeudHaar *racine,int limite)
{
	if(m != NULL)
	{
		// Allocations des fils
		racine->pp =(noeudHaar*)malloc(sizeof(noeudHaar));
		racine->pm =(noeudHaar*)malloc(sizeof(noeudHaar));
		racine->mp =(noeudHaar*)malloc(sizeof(noeudHaar));
		racine->mm =(noeudHaar*)malloc(sizeof(noeudHaar));
		racine->pp->m =racine->pm->m = racine->mp->m = racine->mm->m = NULL;
		// On attribus les matrices au fils
		decoupeImageEn4(m,&(racine->pp->m),&(racine->pm->m),&(racine->mp->m),&(racine->mm->m));

		// On met tout les FILS DES FILS à NULL
		setNoeudHaarFilsNuls(racine->pp);
		setNoeudHaarFilsNuls(racine->mp);
		setNoeudHaarFilsNuls(racine->pm);
		setNoeudHaarFilsNuls(racine->mm);

		// On libère la matrice du noeud sauf si c'est la racine du haut
		if(racine->m != NULL && limite != 0){
			libereMatriceGrise(&(racine->m));
			racine->m = NULL;
		}
		if(limite == 0)
			racine->m = NULL;
	}
}
void setNoeudHaarFilsNuls(noeudHaar *racine)
{
	if(racine != NULL)
	{
		racine->pp = NULL;
		racine->pm = NULL;
		racine->mp = NULL;
		racine->mm = NULL;
	}		
}

void decomposeNNiveaux(matGrise *mat, noeudHaar **racine, int niveau)
{

	if(mat != NULL)
	{
		// Le noeud racine
		noeudHaar *N = (noeudHaar*)malloc(sizeof(noeudHaar));
		int cpt = 0;
		N->m = mat;

		noeudHaar *premier = N;

		if(niveau >= 0)
		{
			for(int i = 0; i < niveau; i++)
			{
				if(N->m->nblig >= 4 && N->m->nbcol >= 4)
				{
					decomposeImage(N->m,N,cpt);
					// Puis on décompose le pp suivant
					N = N->pp;
					cpt++;
				}
				else
					printf("L'image est décomposé à son maximum, en %d fois\n",cpt);			
			}
		}
		else
		{
			while(N->m->nblig >= 4 && N->m->nbcol >= 4)
			{
					decomposeImage(N->m,N,cpt);					
					// Puis on décompose le pp suivant
					N = N->pp;
					cpt++;
			}
			printf("L'image est décomposé à son maximum, en %d fois\n",cpt);
		}	
		*racine = premier;
	}
}

void sauveArbreImage(noeudHaar *N, matGrise **res)
{
	if(N != NULL)
	{
		// Noeud
		if(N->m == NULL)
		{
			// Récuperation de la longueur d'un coté de la future matrice carre 2^N
			// 2 * celle d'un de ses fils
			int size = 2 * N->pm->m->nbcol;

			// Allocation de la matrice
			matGrise *new = NULL; // Probleme, il faut la free celle la après la copie
			alloueMatriceGrise(size, size, &new);
			
			pos p;

			// En bas à gauche : pm
			p.x = p.y = 0;
			copieMatriceGriseDansUneAutreALaPosition(p, new, N->pm->m);
			
			// En haut à droite : mp
			p.x = p.y = (size/2);
			copieMatriceGriseDansUneAutreALaPosition(p, new, N->mp->m);

			// En bas à droite : mm
			p.x = (size/2);
			p.y = 0;
			copieMatriceGriseDansUneAutreALaPosition(p, new, N->mm->m);

			// EN haut à gauche : pp 
			p.x = 0;
			p.y = (size/2);
			// On alloue le futur pp
			matGrise *recursion = NULL;
			// Ne pas allouer recursion, il le sera dans la recursion
			// alloueMatriceGrise(size/2, size/2, &recursion);
			// Récursion car les pp sont des noeuds
			sauveArbreImage(N->pp, &recursion);
			// Puis la copie dans new
			copieMatriceGriseDansUneAutreALaPosition(p, new, recursion);
			// Et on libère recursion car n'est plus utile
			libereMatriceGrise(&recursion);				

			*res = new;
		}
		else // Feuille
		{
			*res = N->m;
		}
	}
}

void recomposeImage(noeudHaar *N,matGrise **res)
{
	if(N != NULL)
	{
		if(N->m == NULL)
		{
			matGrise *newP,*newM,*final;
			newP = newM = final = NULL;

			// printf("On est sur un noeud, on descend jusqu'au dernier pp qui est une feuille\n");
			// La matrice ainsi construite va aller dans pp
			recomposeImage(N->pp,&(N->pp->m));

			recomposeCarreEnRectangle(&newP,N->pp->m,N->pm->m);
			recomposeCarreEnRectangle(&newM,N->mp->m,N->mm->m);

			recomposeRectangleEnCarre(&final,newP,newM);

			*res = final;

			libereMatriceGrise(&newP);
			libereMatriceGrise(&newM);
		}
		else
		{
			*res = N->m;

		}
	}
}

// M1 correspond à C et M2 à D
void recomposeCarreEnRectangle(matGrise **dest, matGrise *m1, matGrise *m2)
{
	if(m1 != NULL && m2 != NULL)
	{
		matGrise *new = NULL;
		alloueMatriceGrise(m1->nblig*2,m1->nbcol,&new);
		for (int i = 0; i < m1->nblig; i++)
		{
			for (int j = 0; j < m1->nbcol; j++)
			{
				new->g[2 * i][j] = m1->g[i][j] + m2->g[i][j];
				new->g[2 * i + 1][j] = m1->g[i][j] - m2->g[i][j];
			}
		}
		*dest = new;
	}
	else
		printf("recomposeCarreEnRectangle : m1 ou m2 est NULL \n");
}

void recomposeRectangleEnCarre(matGrise **dest, matGrise *m1, matGrise *m2)
{
	if(m1 != NULL && m2 != NULL)
	{
		matGrise *new = NULL;
		alloueMatriceGrise(m1->nblig,m1->nbcol*2,&new);
		for (int i = 0; i < m1->nblig; i++)
		{
			for (int j = 0; j < m1->nbcol; j++)
			{
				new->g[i][2 * j] = m1->g[i][j] + m2->g[i][j];
				new->g[i][2 * j + 1] = m1->g[i][j] - m2->g[i][j];
			}
		}
		*dest = new;
	}
	else
		printf("recomposeRectangleEnCarre : m1 ou m2 est NULL \n");
}

void libereArbreHaar(noeudHaar **racine)
{
	noeudHaar *n = *racine;
	if(n != NULL)
	{
		// Si on est sur un noeud
		if(n->pp != NULL)
		{
			// Recursion
			libereArbreHaar(&(n->pp));
			libereArbreHaar(&(n->pm));
			libereArbreHaar(&(n->mp));
			libereArbreHaar(&(n->mm));
		}
		if(n->m != NULL)
			libereMatriceGrise(&(n->m));
		free(n);
		n = NULL;
	}
}