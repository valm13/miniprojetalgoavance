#ifndef ARBRE_H
#define ARBRE_H
/**
 * @brief Vérifie si la matrice est toute noire, toute blanche ou aucune des deux, aux coordonnées indiqués dans le noeud
 * 
 * @param N Noeud
 * @param int Matrice 2D
 * 
 * @return 0 si tout noir, 1 si tout blanc, 2 sinon
 */
int verifieCouleur(noeud *N, short int **g);

/**
 * @brief Crée l'arbre quaternaire
 * 
 * @param m Pointeur sur une matGrise
 * @param racine Adresse d'un pointeur sur un noeud
 * @param xmin Abcisse minimale
 * @param xmax Abcisse maximale
 * @param ymin Ordonnée minimale
 * @param ymax Ordonnée maximale
 */
void creeArbreQuaternaire(matGrise *m, noeud **racine, int xmin, int xmax, int ymin, int ymax);

/**
 * @brief Affiche l'arbre en mode prefixe
 * 
 * @param racine Racine de l'arbre
 */
void affichePrefixe(noeud *racine);

/**
 * @brief Crée une matrice à partir de l'arbre quaternaire en paramètre
 * 
 * @param racine Racine de l'abre quaternaire
 * @param NEW Adresse du pointeur sur une matriceGrise
 * @param etic Boolean : Mettre une valeur différente de 0 si on veut utiliser les valeurs des etiquettes
 */
void creeMatriceFromArbreQuaternaire(noeud *racine, matGrise **NEW, int etic);

/**
 * @brief remplis les valeurs de la matrice
 * 
 * @param racine Noeud racine
 * @param int Matrice 2D
 * @param etic Boolean : Mettre une valeur différente de 0 si on veut utiliser les valeurs des etiquettes
 */
void colorisMatrice(noeud *racine,short int **g, int etic);

/**
 * @brief Coloris la matrice aux positions du noeuds
 * @details Coloris en noir et blanc si etic est à 0, sinon aux valeurs des etiquettes
 * 
 * @param N Noeud actuel
 * @param int Matrice 2D
 * @param etic Boolean : Mettre une valeur différente de 0 si on veut utiliser les valeurs des etiquettes
 */
void colorisNoeud(noeud *N,short int **g, int etic);

/**
 * @brief Libère l'arbre proprement
 * @details Il faut bien entendu passer en paramètre la racine de l'arbre pour bien le libérer
 * 
 * @param racine Racine de l'arbre quaternaire
 */
void libereArbre(noeud **racine);

/**
 * @brief Fonction affichant les valeurs d'une matrice 2D aux coordonnées passées en paramètre
 * 
 * @param int Matrice 2D
 * @param xmin Abcisse minimale
 * @param xmax Abcisse maximale
 * @param ymin Ordonnée minimale
 * @param ymax Ordonnée maximale
 */
void afficheTableau2DEntre(short int **t,int xmin,int xmax,int ymin,int ymax);

/**
 * @brief Attribue les etiquettes à l'arbre quaternaire -1 pour les noeuds sinon une valeur entre 0 et 255
 * 
 * @param racine racine de l'arbre
 * @param nb Valeur de l'étiquette
 */
void etiquetteFeuilleNB(noeud *racine, int *nb);

#endif