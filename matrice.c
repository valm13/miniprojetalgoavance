#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "libISEN/BmpLib.h"
#include "structure.h"
#include "matrice.h"

void creeMatrice(int n, int m, short int ***mat, unsigned char *data, int ptr)
{
	short int **Mat = NULL;
	Mat = (short int**)malloc(n * sizeof(short int*));
	for(int i = 0; i < n; i++){
		Mat[i] = (short int *)malloc(m*sizeof(short int));

		for(int j = 0; j < m; j++)
		{
			Mat[i][j] = (short int)data[ptr];
			ptr += 3;
		}
	}
	*mat = Mat;
}
void libereMatrice(int nblig, short int **t)
{
	for(int i = 0; i < nblig; i++)
	{
		free(t[i]);
		t[i] = NULL;
	}
	free(t);
	t = NULL;
}

void alloue3Matrice(int n, int m, matImage **new)
{
	matImage *NEW = NULL;
	// Allocation de la nouvelle matrice
	NEW = (matImage*)malloc(sizeof(matImage));
	NEW->nblig = n;
	NEW->nbcol = m;
	NEW->r = (short int **)malloc(n*sizeof(short int *));
	NEW->v = (short int **)malloc(n*sizeof(short int *));
	NEW->b = (short int **)malloc(n*sizeof(short int *));
	for(int i = 0; i < n ; i++)
	{
		NEW->r[i] = (short int *)malloc(m*sizeof(short int));
		NEW->v[i] = (short int *)malloc(m*sizeof(short int));
		NEW->b[i] = (short int *)malloc(m*sizeof(short int));
	}
	*new = NEW;
}

void cree3Matrices(DonneesImageRGB *img, matImage **m)
{
	if(*m != NULL)
		libere3Matrices(m);
	int h = img->hauteurImage;
	int l = img->largeurImage;
	matImage *mat = NULL;
	if(img != NULL)
	{
		mat = (matImage*)malloc(sizeof(matImage));
		mat->nblig = h;
		mat->nbcol = l;

		creeMatrice(h,l,&(mat->b),img->donneesRGB,0);
		creeMatrice(h,l,&(mat->v),img->donneesRGB,1);
		creeMatrice(h,l,&(mat->r),img->donneesRGB,2);
		// printf("Matrice created with success\n");
	}
	*m = mat;
}

void libere3Matrices(matImage **m)
{
	matImage *M = *m;

	if(M != NULL)
	{
		libereMatrice(M->nblig,M->r);
		libereMatrice(M->nblig,M->v);
		libereMatrice(M->nblig,M->b);
		free(M);
		M = NULL;
		// printf("MatImage deleted with success\n");
	}
	*m = M;
}

void affiche3Mat(matImage *m)
{
	printf("Affichage de la matrice de %d lignes et %d colonnes\n", m->nblig,m->nbcol);
	if(m != NULL)
	{
		for(int i = 0; i < m->nblig; i++)
			for(int j = 0; j < m->nbcol; j++)
				printf("Pixel [%d][%d] : B = %d | V = %d | R = %d",i, j, m->b[i][j],m->v[i][j],m->r[i][j]);

	}
	else
		printf("Matrice NULL\n");
}

void alloueMatriceGrise(int n, int m, matGrise **new)
{
	// printf("Allocation\n");
	if(*new != NULL)
		libereMatriceGrise(new);
	matGrise *NEW = NULL;
	// Allocation de la nouvelle matrice
	NEW = (matGrise*)malloc(sizeof(matGrise));
	NEW->nblig = n;
	NEW->nbcol = m;
	NEW->g = (short int **)malloc(n*sizeof(short int *));
	for(int i = 0; i < n ; i++)
		NEW->g[i] = (short int *)malloc(m*sizeof(short int));
	*new = NEW;
}

void libereMatriceGrise(matGrise **mat)
{
	matGrise *m = *mat;
	if(m != NULL)
	{
		// printf("%d\n",m->nblig);
		libereMatrice(m->nblig, m->g);
		free(m);
		m = NULL;
		*mat = m;
		// printf("MatGrise deleted with success\n");
	}
}

void copieMatriceGriseDansUneAutreALaPosition(pos p, matGrise *dest, matGrise *src)
{
	if(dest != NULL && src != NULL)
	{
		if(src->nbcol <= dest->nbcol && src->nblig <= dest->nblig)
		{
			if(p.x + src->nbcol <= dest->nbcol && p.y + src->nblig <= dest->nblig)
			{
				for (int i = 0; i < src->nblig; i++)
				{
					for (int j = 0; j < src->nbcol; j++)
					{
						dest->g[i+p.y][j+p.x] = src->g[i][j];
					}	

				}
			}
			else
				printf("p est mal positionné\n");
		}
		else
			printf("Impossible de copier une matrice dans une plus petite\n");
	}
	else
		printf("Une des 2 matrice n'est pas initialisée\n");
}

void copieMatImageDansUneAutreALaPosition(pos p, matImage *dest, matImage *src)
{
	if(dest != NULL && src != NULL)
	{
		if(src->nbcol <= dest->nbcol && src->nblig <= dest->nblig)
		{
			if(p.x + src->nbcol <= dest->nbcol && p.y + src->nblig <= dest->nblig)
			{
				for (int i = 0; i < src->nblig; i++)
				{
					for (int j = 0; j < src->nbcol; j++)
					{
						dest->r[i+p.y][j+p.x] = src->r[i][j];
						dest->v[i+p.y][j+p.x] = src->v[i][j];
						dest->b[i+p.y][j+p.x] = src->b[i][j];
					}	

				}
			}
			else
				printf("p est mal positionné\n");
		}
		else
			printf("Impossible de copier une matrice dans une plus petite\n");
	}
	else
		printf("Une des 2 matrice n'est pas initialisée\n");
}

void matGriseTo3Mat(matGrise *source, matImage **dest)
{
	if(*dest != NULL)
		libere3Matrices(dest);
	alloue3Matrice(source->nblig, source->nbcol, dest);
	for(int i = 0; i < source->nblig; i++)
			for(int j = 0; j < source->nbcol; j++)
				(*dest)->r[i][j] = (*dest)->v[i][j] = (*dest)->b[i][j] = source->g[i][j];
}

int verifie3MatBinaire(matImage *source)
{
	for(int i = 0; i < source->nblig; i++)
			for(int j = 0; j < source->nbcol; j++)
				if((source->r[i][j] > 0 && source->r[i][j] < 255) || (source->v[i][j] > 0 && source->v[i][j] < 255) || (source->b[i][j] > 0 && source->b[i][j] < 255))
					return 0;
	return 1;
}

int verifieMatGriseBinaire(matGrise *source)
{
	for(int i = 0; i < source->nblig; i++)
			for(int j = 0; j < source->nbcol; j++)
				if((source->g[i][j] > 0 && source->g[i][j] < 255))
					return 0;
	return 1;
}
