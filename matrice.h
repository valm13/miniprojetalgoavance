#ifndef MAT_H
#define MAT_H
#include "libISEN/BmpLib.h"
// Gestion Matricielle

/**
 * @brief Alloue un tableau 2D dynamiquement et le remplis à l'aide d'une image
 * 
 * @param n Hauteur
 * @param m Largeur
 * @param int Adresse de la matrice
 * @param char tableau 1D correspondant aux donnees de l'image
 * @param ptr Valeur entre 0 et 2, permet de choisir la couleur à récuperer dans la matrice 
 */
void creeMatrice(int n, int m, short int ***mat, unsigned char *data, int ptr);

/**
 * @brief Libere une matrice
 * 
 * @param nblig Hauteur de la matrice
 * @param int Matrice
 */
void libereMatrice(int nblig, short int **t);

// Gestion des structures matImage et matGrise
/**
 * @brief Alloue une structure matImage composée de 3 matrices : r,v,b aux dimensions (n,m)
 * 
 * @param n Hauteur
 * @param m Largeur
 * @param new Double pointeur sur une matImage
 */
void alloue3Matrice(int n, int m, matImage **new);

/**
 * @brief Crée une matImage dynamiquement à l'aide du Image
 * 
 * @param img Image
 * @param m Double pointeur sur une matImage
 */
void cree3Matrices(DonneesImageRGB *img, matImage **m);

/**
 * @brief Libere toutes les données d'une matImage
 * 
 * @param m Double pointeur sur une matImage
 */
void libere3Matrices(matImage **m);

/**
 * @brief Affiche le contenu d'une matImage
 * 
 * @param m Pointeur sur une matImage
 */
void affiche3Mat(matImage *m);

/**
 * @brief Alloue une matrice grise
 * @details Une matrice grise n'a qu'une composante
 * 
 * @param n Hauteur
 * @param m Largeur
 * @param new Double pointeur sur une matGrise
 */
void alloueMatriceGrise(int n, int m, matGrise **new);

/**
 * @brief Libere une matrice grise
 * 
 * @param mat Double pointeur sur une matGrise
 */
void libereMatriceGrise(matGrise **mat);

/**
 * @brief Permet de copier une matrice grise dans une autre à la position p
 * 
 * @param p Position de la forme p.x et p.y
 * @param dest MatGrise de destination
 * @param src MatGrise à copié
 */
void copieMatriceGriseDansUneAutreALaPosition(pos p, matGrise *dest, matGrise *src);

/**
 * @brief Permet de copier une MatImage dans une autre à la position p
 * 
 * @param p Position de la forme p.x et p.y
 * @param dest MatImage de destination
 * @param src MatImage à copier
 */
void copieMatImageDansUneAutreALaPosition(pos p, matImage *dest, matImage *src);

/**
 * @brief      { Crée une matImage à partir d'une matrice grise }
 *
 * @param      source  MatGrise source
 * @param      dest    MatImage de destination
 */
void matGriseTo3Mat(matGrise *source, matImage **dest);

/**
 * @brief Verifie si la 3mat contient que des valeurs binaires (0 ou 255)
 * @details Renvois 1 si vrai et 0 si faux
 * 
 * @param source Pointeur sur matImage
 * @return Renvois un int à 1 si vrai et 0 si faux
 */
int verifie3MatBinaire(matImage *source);

/**
 * @brief Verifie si la matGrise contient que des valeurs binaires (0 ou 255)
 * @details Renvois 1 si vrai et 0 si faux
 * 
 * @param source Pointeur sur matGrise
 * @return Renvois un int à 1 si vrai et 0 si faux
 */
int verifieMatGriseBinaire(matGrise *source);
#endif