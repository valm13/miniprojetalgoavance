#ifndef TRANSFO_H
#define TRANSFO_H
/**
 * @brief Fait le négatif d'une matImage
 * @details Prend une matImage source et fait son négatif dans une nouvelle matImage
 * 
 * @param src matImage source
 * @param new matImage destination
 */
void negatifImage(matImage *src, matImage **new);

/**
 * @brief Fait le niveau de gris d'une matImage
 * @details L'enregistre dans une matGrise
 * 
 * @param src Pointeur sur MatImage
 * @param new Double Pointeur sur une matGrise
 */
void couleur2NG(matImage *src, matGrise **new);

/**
 * @brief Effectue le seuillage d'une image en niveau de gris
 * 
 * @param src Pointeur sur matGrise
 * @param new Double Pointeur sur une matGrise
 * @param seuil Seuil compris entre 0 et 255
 */
void seuillageNG(matGrise *src, matGrise **new, int seuil);

/**
 * @brief Effectue le filtre médian de l'image
 * @details A Pour but d'éliminer le bruit sur une image en niveau de gris
 * 
 * @param src Pointeur sur matGrise
 * @param new Double Pointeur sur une matGrise
 */
void filtreMedian(matGrise *src, matGrise **new);
#endif