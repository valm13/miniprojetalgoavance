#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h> // Pour lister les fichiers
#include "structure.h"
#include "matrice.h"
#include "image.h"
#include "transformation.h"
#include "arbre.h"
#include "haar.h"
#include "compression.h"
#include "menu.h"

void menuPrincipal(int *choix, matImage **m_source, matImage **m_dest, matGrise **mg, noeudHaar **n)
{
	printf("\n------------Menu------------\n\n");
	printf("1 - Charger une image\n");
	printf("2 - Transformation\n");
	printf("3 - Compresser\n");
	printf("4 - Decompresser\n");
	printf("5 - Sauvegarder\n");
	printf("6 - Decomposer / Recomposer la matrice source\n");
	printf("7 - Autre\n");
	printf("8 - Quitter\n\n");

	do
	{
		printf("Veuillez selectionner un nombre entre 1 et 8 :\n");
	 
		scanf("%d",choix);
	}while(*choix < 1 || *choix > 8);

		switch(*choix)
		{
			case 1:
				menuChargement(m_source);

				// On libère le reste
				libere3Matrices(m_dest);
				libereMatriceGrise(mg);
			break;

			case 2:
				if(*m_source != NULL)
					menuTransformation(*m_source, m_dest, mg);
				else
					printf("WARNING : Aucune image n'est chargee, veuillez en charger une.\n");					
			break;

			case 3:
				if(*m_source != NULL)
					menuCompression(*m_source, *m_dest, *mg);
				else
					printf("WARNING : Aucune image n'est chargee, veuillez en charger une.\n");				
			break;

			case 4:
				menuDecompression(m_source, m_dest, mg);
			break;

			case 5:
				menuSauvegarde(*m_source, *m_dest, *mg);
			break;

			case 6:
				menuDecompRecomp(m_source, m_dest, mg, n);
			break;

			case 7:
				menuAutre(m_source, m_dest, mg);
			break;

			case 8:
				printf("Au revoir !\n");
			break;

			default:
				printf("WARNING : Le choix que vous avez fait est incorrect\n");
			break;
		}
}

void menuChargement(matImage **m_source)
{
	DonneesImageRGB *img = NULL;
	
	char choix[50];
 	char path[200];
 	strcpy(path,"images/source/");

 	// Listing des images chargeables
 	printf("INFO : Images Sources disponibles\n");
	listeFichiersFrom("./images/source/"); 
	printf("Veuillez rentrer le nom de l'image à charger sans l'extension\n");

	scanf("%s",choix);

	strcat(path,choix);
	strcat(path,".bmp");

	img = lisBMPRGB(path);
	if(img != NULL)
	{
		cree3Matrices(img,m_source);
		libereDonneesImageRGB(&img);
		printf("La matrice a ete correctement chargee\n");
	}
	else
		printf("WARNING : Problème de chargement : l'image n'existe pas\n");
	
}

void menuTransformation(matImage *m_source, matImage **m_dest, matGrise **mg)
{
	int choix,seuil;
	int choixAutre = 0;
	matGrise *temp, *temp2;
	temp = temp2 = NULL;
	noeud *racine = NULL;
	noeudHaar *racineHaar = NULL;
	printf("Quelle est la transformation que vous souhaitez realiser ?\n");
	printf("1 - Negatif\n");
	printf("2 - Niveau de gris\n");
	printf("3 - Seuillage\n");
	printf("4 - Median\n");
	printf("5 - Arbre quaternaire\n");
	printf("6 - Arbre Haar\n");
	printf("7 - Retour\n");

	do
	{
		printf("Veuillez selectionner un nombre entre 1 et 7 :\n");
	
		scanf("%d",&choix);
	}while(choix < 1 || choix > 7);

		switch(choix)
		{
			case 1:
				if(m_source != NULL)
				{
					negatifImage(m_source, m_dest);
					printf("SUCCEED : Negatif\n");
				}
				else
					printf("WARNING : Veuillez d'abord charger une image\n");
			break;

			case 2:
				if(m_source != NULL)
				{
					couleur2NG(m_source, mg);
					printf("SUCCEED : Niveau de gris\n");

				}
				else
					printf("WARNING : Veuillez d'abord charger une image\n");
			break;

			case 3:
				if(m_source != NULL)
				{

					couleur2NG(m_source, &temp);
					do
					{
						printf("Veuillez choisir un seuil compris entre 0 et 255\n");
						scanf("%d",&seuil);
					}while(seuil < 0 || seuil > 255);
					seuillageNG(temp, mg, seuil);
					printf("SUCCEED : Seuil à %d\n",seuil);
				}
				else
					printf("WARNING : Veuillez d'abord charger une image\n");
			break;

			case 4:
				if(m_source != NULL)
				{	
					printf("Combien d'application du filtre median voulez vous effectuer ?\n");
					scanf("%d",&choixAutre);
					if(choixAutre > 0)
					{
						couleur2NG(m_source, &temp);
						for (int i = 0; i < choixAutre; i++)
						{
							filtreMedian(temp, &temp2);
							libereMatriceGrise(&temp);
							temp = temp2;
							temp2 = NULL;
						}
						*mg = temp;
						temp = NULL;
					}
					printf("SUCCEED : Median\n");
				}
				else
					printf("WARNING : Veuillez d'abord charger une image\n");
			break;

			case 5:
				if(m_source != NULL)
				{
					if(m_source->nblig == m_source->nbcol && m_source->nblig % 2 == 0)
					{
						printf("Version etiquette : 1\n");
						printf("Version sans etiquette : 0\n");
						printf("Faites votre choix\n");
						scanf("%d",&choixAutre);
						if(choixAutre)
							choixAutre = 1;
						couleur2NG(m_source, &temp);
						do{
							printf("Cette transformation nécessite une image en noir et blanc, veuillez choisir un seuillage compris entre 0 et 255\n");
							scanf("%d",&seuil);
						}while(seuil);
						seuillageNG(temp, mg, seuil);

						creeArbreQuaternaire(*mg, &racine, 0, temp->nbcol-1, 0, temp->nblig-1);
						creeMatriceFromArbreQuaternaire(racine, mg, choixAutre);

						libereArbre(&racine);

						printf("SUCCEED : Arbre quaternaire\n");
					}
					else
						printf("WARNING : L'image source doit être carré et de taille 2^N\n");
					
				}
				else
					printf("WARNING : Veuillez d'abord charger une image\n");
			break;

			case 6:
				if(m_source != NULL)
				{
					if(m_source->nblig == m_source->nbcol && m_source->nblig % 2 == 0)
					{
						couleur2NG(m_source, mg);

						printf("Veuillez rentrer le nombre de décomposition, -1 : maximum\n");
						scanf("%d",&choixAutre);

						decomposeNNiveaux(*mg,&racineHaar,choixAutre);
						libereMatriceGrise(mg);

						sauveArbreImage(racineHaar,mg); // Il y avait des fuites mémoires, mais normalement c'est corrigé
						libereArbreHaar(&racineHaar);

						racineHaar = NULL;

						printf("SUCCEED : Arbre Haar\n");
					}
					else
						printf("WARNING : L'image source doit être carré et de taille 2^N\n");
				}
				else
					printf("WARNING : Veuillez d'abord charger une image\n");
			break;

			case 7:
				printf("INFO : Vous n'avez pas effectué de transformation\n");
			break;

			default:
				printf("WARNING : Le choix que vous avez fait est incorrect\n");
			break;
		}
	libereMatriceGrise(&temp);
}

void menuCompression(matImage *m_source, matImage *m_dest, matGrise *mg)
{
	matGrise *temp = NULL;
	noeud *racine = NULL;
	int choix,choixAutre;
	char path[1000];

	printf("Quelle type de compression voulez vous effectuer ?\n");
	printf("1 - Binaire\n");
	printf("2 - Niveau de gris\n");
	printf("3 - Retour\n");

	do
	{
		printf("Veuillez selectionner un nombre entre 1 et 3 :\n");
		scanf("%d",&choix);
	}while(choix < 1 || choix > 3);

		switch(choix)
		{
			case 1:
				if(m_source != NULL || m_dest != NULL || mg != NULL)
				{
					printf("Quelle matrice voulez vous compresser ?\n");
					printf("1 - Source\n");
					printf("2 - Dest\n");
					printf("3 - Grise\n");
					printf("4 - Retour\n");
					do{
						scanf("%d",&choixAutre);
					}while(choixAutre < 1 || choixAutre > 4);

					switch(choixAutre)
					{
						case 1:
							if(m_source != NULL)
							{
								if(verifie3MatBinaire(m_source))
								{
									if(m_source->nbcol % 2 == 0)
									{
										couleur2NG(m_source, &temp);
										creeArbreQuaternaire(temp, &racine, 0, temp->nbcol-1, 0, temp->nblig-1);
										destinationCompressionBinaire(path);
										compressionBinaireSP(path, racine);
										libereArbre(&racine);
										printf("SUCCEED : Compression binaire de la matrice source effectuée\n");
									}
									else
										printf("La matrice n'est pas de taille 2^N\n");

									
								}
								else
									printf("La matrice source n'est pas binaire (en noir et blanc)\n");								
							}
							else
								printf("WARNING : La matrice source n'est pas chargée\n");
						break;

						case 2:
							if(m_dest != NULL)
							{
								if(verifie3MatBinaire(m_dest))
								{
									if(m_dest->nbcol % 2 == 0)
									{
										couleur2NG(m_dest, &temp);
										creeArbreQuaternaire(temp, &racine, 0, temp->nbcol-1, 0, temp->nblig-1);
										destinationCompressionBinaire(path);
										compressionBinaireSP(path, racine);
										libereArbre(&racine);
										printf("SUCCEED : Compression binaire de la matrice dest effectuée\n");
									}
									else
										printf("La matrice n'est pas de taille 2^N\n");									
								}
								else
									printf("La matrice dest n'est pas binaire (en noir et blanc)\n");
								
							}
							else
								printf("WARNING : La matrice dest n'est pas chargée\n");
						break;

						case 3:
							if(mg != NULL)
							{
								if(verifieMatGriseBinaire(mg))
								{
									if(mg->nbcol % 2 == 0)
									{
										creeArbreQuaternaire(mg, &racine, 0, mg->nbcol-1, 0, mg->nblig-1);
										destinationCompressionBinaire(path);
										compressionBinaireSP(path, racine);
										libereArbre(&racine);
										printf("SUCCEED : Compression binaire de la matrice grise effectuée\n");
									}
									else
										printf("La matrice n'est pas de taille 2^N\n");
									
								}
								else
									printf("La matrice grise n'est pas binaire (en noir et blanc)\n");	
							}
							else
								printf("WARNING : La matrice grise n'est pas chargée\n");
						break;

						case 4:
							printf("INFO : Vous n'avez rien compressé\n");
						break;
					}


				}
				else
					printf("WARNING : Veuillez d'abord charger une image\n");
			break;

			case 2:
				if(0)
				{
					
				}
				else
					printf("INFO : La compression d'image en niveau de gris n'est pas implementée\n");
			break;

			case 3:
				printf("INFO : Vous n'avez rien compressé\n");
			break;

			default:
				printf("WARNING : Le choix que vous avez fait est incorrect\n");
			break;
		}

	libereMatriceGrise(&temp);
}

void menuDecompression(matImage **m_source, matImage **m_dest, matGrise **mg)
{
	int choix;
	matGrise *temp = NULL;
	char nom[100];

	printf("Quelle type de décompression voulez vous effectuer ?\n");
	printf("1 - Binaire\n");
	printf("2 - Niveau de gris\n");
	printf("3 - Retour\n");

	do
	{
		printf("Veuillez selectionner un nombre entre 1 et 3 :\n");
	
		scanf("%d",&choix);
	}while(choix < 1 || choix > 3);

		switch(choix)
		{
			case 1:
			 	// Listing des images chargeables
			 	printf("INFO : Fichiers binaires d'image binaire disponibles\n");
				listeFichiersFrom("./binaires/image_binaire/"); 
				printf("Veuillez rentrer le nom du fichier binaire à charger sans l'extension\n");

				scanf("%s",nom);
				if(decompressionBinaireSP(nom, &temp))
				{
					matGriseTo3Mat(temp, m_source);
					printf("coucou3\n");

					if(*m_source != NULL)
					{
						libere3Matrices(m_dest);
						libereMatriceGrise(mg);
						printf("SUCCEED : La décompression a bien été effectué\n");
					}
					else
						printf("WARNING : La décompression n'a pas bien été effectué\n");
				}
				else
					printf("WARNING : Le fichier portant ce nom n'existe pas\n");
				
			break;

			case 2:
				if(0)
				{
					
				}
				else
					printf("INFO : La décompression d'image en niveau de gris n'est pas implementée\n");
			break;

			case 3:
				printf("INFO : Vous n'avez rien décompressé\n");
			break;

			default:
				printf("WARNING : Le choix que vous avez fait est incorrect\n");
			break;
		}
	libereMatriceGrise(&temp);
}

void menuSauvegarde(matImage *m_source, matImage *m_dest, matGrise *mg)
{
	DonneesImageRGB *img = NULL;
	int choix;
	char path[100];
	printf("Quelle matrice voulez vous sauvegarder ?\n");
	printf("1 - La matrice couleur source\n");
	printf("2 - La matrice couleur dest (soit le négatif)\n");
	printf("3 - La matrice grise\n");
	printf("4 - Retour\n");

	do
	{
		printf("Veuillez selectionner un nombre entre 1 et 3 :\n");
	
		scanf("%d",&choix);
	}while(choix < 1 || choix > 4);

		switch(choix)
		{
			case 1:
				if(m_source != NULL)
				{
					destinationImage(path);
					creeImageFromMatImage(m_source,&img);
					sauvegardeImage(img, path);
					libereDonneesImageRGB(&img);
					printf("SUCCEED : Image sauvegardée : %s\n",path );
				}
				else
					printf("WARNING : La matrice source est vide\n");
			break;

			case 2:
				if(m_dest != NULL)
				{
					destinationImage(path);
					creeImageFromMatImage(m_dest,&img);
					sauvegardeImage(img, path);
					libereDonneesImageRGB(&img);
					printf("SUCCEED : Image sauvegardée : %s\n",path );
				}
				else
					printf("WARNING : La matrice dest est vide\n");
			break;

			case 3:
				if(mg != NULL)
				{
					destinationImage(path);
					creeImageFromMatGrise(mg,&img);
					sauvegardeImage(img, path);
					libereDonneesImageRGB(&img);
					printf("SUCCEED : Image sauvegardée : %s\n",path );
				}
				else
					printf("WARNING : La matrice grise est vide\n");
			break;
			case 4:
					printf("INFO : Vous n'avez rien sauvegardé\n");
			break;

			default:
				printf("WARNING : Le choix que vous avez fait est incorrect\n");
			break;
		}
}

void menuDecompRecomp(matImage **m_source, matImage **m_dest, matGrise **mg, noeudHaar **racine)
{
	int choix,niveau;
	matGrise *temp = NULL;
	printf("Vous voulez décomposer ou recomposer l'image source ? \n");
	printf("1 - Décomposer l'image source\n");
	printf("2 - Recomposer dans l'image source\n");
	printf("3 - Retour\n");
	do{
		scanf("%d",&choix);
	}while(choix < 1 || choix > 3);

	switch(choix)
	{
		case 1:
			if(*m_source != NULL)
			{
				printf("Quel niveau de décomposition ?\n");
				printf("-1 --> décompose au maximum");
				scanf("%d",&niveau);
				couleur2NG(*m_source, &temp);
				if(*racine != NULL)
					libereArbreHaar(racine);
				decomposeNNiveaux(temp, racine, niveau);
				libereMatriceGrise(&temp);
				printf("SUCCEED : La décomposition a bien été effectuée \n");
			}
			else
				printf("Vous n'avez pas chargé d'image\n");
		break;

		case 2:
			if(*racine != NULL)
			{
				recomposeImage(*racine, &temp);
				matGriseTo3Mat(temp, m_source);
				libereMatriceGrise(mg);
				libere3Matrices(m_dest);
				printf("SUCCEED : La recomposition dans l'image source a bien été effectuée \n");
			}
			else
				printf("Vous n'avez pas décomposé d'image\n");
		break;

		case 3:
			printf("INFO : Retour\n");
		break;
	}
}
void menuAutre(matImage **m_source, matImage **m_dest, matGrise **mg)
{
	int choix;
	printf("Vous voulez remplacer l'image source par l'image dest ou l'image grise ?\n");
	printf("1 - L'image dest\n");
	printf("2 - L'image grise\n");
	printf("3 - Retour\n");

	do
	{
		printf("Veuillez selectionner un nombre entre 1 et 3 :\n");
	
		scanf("%d",&choix);
	}while(choix < 1 || choix > 3);
		
		switch(choix)
		{
			case 1:
				if(*m_dest != NULL)
				{
					libere3Matrices(m_source);
					*m_source = *m_dest;

					libereMatriceGrise(mg);
					libere3Matrices(m_dest);
					*mg = NULL;
					*m_dest = NULL;

					printf("Vous avez remplace l'image source par l'image dest\n");
				}
				else
					printf("WARNING : La matrice dest est vide\n");
			break;

			case 2:
				if(*mg != NULL)
				{
					libere3Matrices(m_source);
					matGriseTo3Mat(*mg, m_source);

					libereMatriceGrise(mg);
					libere3Matrices(m_dest);
					*mg = NULL;
					*m_dest = NULL;

					printf("Vous avez remplace l'image source par l'image grise\n");
				}
				else
					printf("WARNING : La matrice grise est vide\n");
			break;

			case 3:
				printf("INFO : Vous n'avez rien remplace\n");
			break;

			default:
				printf("WARNING : Le choix que vous avez fait est incorrect\n");
			break;
		}
}

void destinationImage(char *path)
{
	char nom[20];
	strcpy(path,"images/destination/");

	printf("Veuillez rentrer le nom de la future image, celui-ci doit faire moins de 20 caractères :\n");

	scanf("%s",nom);

	strcat(path,nom);
	strcat(path,".bmp");
}

void destinationCompressionBinaire(char *path)
{
	char nom[20];
	strcpy(path,"binaires/image_binaire/");

	printf("Veuillez rentrer le nom du futur fichier binaire contenant la compression de l'image binaire, celui-ci doit faire moins de 20 caractères :\n");

	scanf("%s",nom);

	strcat(path,nom);
	strcat(path,".bin");
}

void listeFichiersFrom(char *path)
{
	DIR * rep = opendir(path); 
  
    if (rep != NULL) 
    { 
        struct dirent * ent; 
  
        while ((ent = readdir(rep)) != NULL) 
        { 
        	if(ent->d_name[0] != '.')
            printf("%s\n", ent->d_name); 
        } 
  
        closedir(rep); 
    } 
}