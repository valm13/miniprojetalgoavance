#ifndef HAAR_H
#define HAAR_H
/**
 * @brief Découpe la matrice en 4 parties pp,pm,mp,mm
 * 
 * @param m Matrice Grise à découpé
 * @param pp Matrice correspondant à la moyenne des moyenne
 * @param pm Matrice correspondant à l'erreur des moyennes
 * @param mp Matrice correspondant à la moyenne des erreurs
 * @param mm Matrice correspondant à l'erreur des erreurs
 */
void decoupeImageEn4(matGrise *m,matGrise **pp,matGrise **pm,matGrise **mp,matGrise **mm);

/**
 * @brief Fais la moyenne des pixels ou l'erreur des pixels dans le sens horizontal
 * 
 * @param Base Matrice de base
 * @param res Matrice résultat
 * @param operation Si 1 on fait la moyenne des pixels et si 0 on fait l'erreur des pixels
 */
void demiMatriceHorizontale(matGrise *Base, matGrise *res, int operation);

/**
 * @brief Fais la moyenne des pixels ou l'erreur des pixels dans le sens vertical
 * 
 * @param Base Matrice de base
 * @param res Matrice résultat
 * @param operation Si 1 on fait la moyenne des pixels et si 0 on fait l'erreur des pixels
 */
void demiMatriceVerticale(matGrise *Base, matGrise *res, int operation);

/**
 * @brief Fonction valeur absolue
 * 
 * @param int Entier short
 * @return Valeur absolue de l'entier short
 */
short int valeurAbsolue(short int v);

/**
 * @brief Décompose un niveau d'image dans l'arbre
 * 
 * @param m Matrice grise
 * @param racine Noeud de l'arbre
 * @param limite Egal à 0 quand on est tout en haut de l'arbre : permet de ne pas libérer l'image grise de départ
 */
void decomposeImage(matGrise *m, noeudHaar *racine, int limite);

/**
 * @brief Met les fils d'un noeud Haar à NULL
 * 
 * @param racine noeud Haar dont les fils seront à NULL
 */
void setNoeudHaarFilsNuls(noeudHaar *racine);

/**
 * @brief Décompose N niveau de l'arbre
 * @details Si on met un niveau de décomposition négatif, on décompose au maximum
 * 
 * @param mat Matrice à décomposer
 * @param racine Adresse du pointeur de la racine de l'arbre
 * @param niveau Niveau de décomposition
 */
void decomposeNNiveaux(matGrise *mat, noeudHaar **racine, int niveau);

/**
 * @brief Sauvegarde une image correspondant à l'arbre : pp en haut à gauche
 * @details pp dans le quart haut gauche, pm dans le quart bas gauche, mp dans le quart haut droite, mm dans le quart bas droite
 * 
 * @param N Racine de l'arbre
 * @param res Adresse du pointeur sur une matGrise
 */
void sauveArbreImage(noeudHaar *N, matGrise **res);

/**
 * @brief Recompose l'image à partir de l'arbre
 * @details Not Working : apparition de petits points noirs et détérioration de l'image lors de la recomposition
 * 
 * @param N Racine de l'arbre
 * @param res Adresse du pointeur sur une matGrise
 */
void recomposeImage(noeudHaar *N,matGrise **res);

/**
 * @brief Recompose 2 matrices grises carré en 1 rectangulaire
 * 
 * @param dest Futur matrice rectangulaire
 * @param m1 Matrice carré 1
 * @param m2 Matrice Carré 2
 */
void recomposeCarreEnRectangle(matGrise **dest, matGrise *m1, matGrise *m2);

/**
 * @brief Recompose 2 matrices grises rectangulaires en 1 carré
 * 
 * @param dest Futur matrice carré
 * @param m1 Matrice Rectangulaire 1
 * @param m2 Matrice Rectangulaire 2
 */
void recomposeRectangleEnCarre(matGrise **dest, matGrise *m1, matGrise *m2);

/**
 * @brief Libère proprement l'arbre de Haar
 * 
 * @param racine Adresse du pointeur sur la racine de l'arbre de Haar
 */
void libereArbreHaar(noeudHaar **racine);
#endif