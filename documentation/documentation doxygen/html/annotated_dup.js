var annotated_dup =
[
    [ "coord", "structcoord.html", "structcoord" ],
    [ "dataFeuille", "structdata_feuille.html", "structdata_feuille" ],
    [ "dataFeuilleHaar", "structdata_feuille_haar.html", "structdata_feuille_haar" ],
    [ "enTete", "structen_tete.html", "structen_tete" ],
    [ "matGrise", "structmat_grise.html", "structmat_grise" ],
    [ "matImage", "structmat_image.html", "structmat_image" ],
    [ "noeud", "structnoeud.html", "structnoeud" ],
    [ "noeudHaar", "structnoeud_haar.html", "structnoeud_haar" ],
    [ "pos", "structpos.html", "structpos" ]
];