var matrice_8h =
[
    [ "affiche3Mat", "matrice_8h.html#a6126054b6951b091979c9c9106084b72", null ],
    [ "alloue3Matrice", "matrice_8h.html#a670d75f4d8af168950c5cf06a7a2c7c9", null ],
    [ "alloueMatriceGrise", "matrice_8h.html#a825e0a2f2ca42777abbb9f4115906ee4", null ],
    [ "copieMatImageDansUneAutreALaPosition", "matrice_8h.html#a5ee83c6386d417971c7c0e54c45edc21", null ],
    [ "copieMatriceGriseDansUneAutreALaPosition", "matrice_8h.html#a5fb822ba442ac6a2b08e60152d108b82", null ],
    [ "cree3Matrices", "matrice_8h.html#ab88b4b1d838fefc927fde625fd1fe72a", null ],
    [ "creeMatrice", "matrice_8h.html#a2fd6dda33723b39a67c3bf62d97d87c4", null ],
    [ "libere3Matrices", "matrice_8h.html#a911eafad9049a6986744207a0d3ad283", null ],
    [ "libereMatrice", "matrice_8h.html#adbafcbb41301101004f4cb83f1d8ab7e", null ],
    [ "libereMatriceGrise", "matrice_8h.html#a707960a9237dcafefbb5ad59604f41fd", null ],
    [ "matGriseTo3Mat", "matrice_8h.html#aebc81cea746bc0602a00a6e3b7359458", null ],
    [ "verifie3MatBinaire", "matrice_8h.html#a8521b44ba1d523743a26d7f354333226", null ],
    [ "verifieMatGriseBinaire", "matrice_8h.html#a652b3386e16fce7c7d234c0489b618dd", null ]
];