var files =
[
    [ "arbre.c", "arbre_8c.html", "arbre_8c" ],
    [ "arbre.h", "arbre_8h.html", "arbre_8h" ],
    [ "compression.c", "compression_8c.html", "compression_8c" ],
    [ "compression.h", "compression_8h.html", "compression_8h" ],
    [ "haar.c", "haar_8c.html", "haar_8c" ],
    [ "haar.h", "haar_8h.html", "haar_8h" ],
    [ "image.c", "image_8c.html", "image_8c" ],
    [ "image.h", "image_8h.html", "image_8h" ],
    [ "main.c", "main_8c.html", "main_8c" ],
    [ "matrice.c", "matrice_8c.html", "matrice_8c" ],
    [ "matrice.h", "matrice_8h.html", "matrice_8h" ],
    [ "menu.c", "menu_8c.html", "menu_8c" ],
    [ "menu.h", "menu_8h.html", "menu_8h" ],
    [ "structure.h", "structure_8h.html", "structure_8h" ],
    [ "transformation.c", "transformation_8c.html", "transformation_8c" ],
    [ "transformation.h", "transformation_8h.html", "transformation_8h" ],
    [ "tri.c", "tri_8c.html", "tri_8c" ],
    [ "tri.h", "tri_8h.html", "tri_8h" ]
];