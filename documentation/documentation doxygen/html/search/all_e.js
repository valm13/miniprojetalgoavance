var searchData=
[
  ['sauvearbreimage',['sauveArbreImage',['../haar_8c.html#a3a0777657d47a7639d712914bededa15',1,'sauveArbreImage(noeudHaar *N, matGrise **res):&#160;haar.c'],['../haar_8h.html#a3a0777657d47a7639d712914bededa15',1,'sauveArbreImage(noeudHaar *N, matGrise **res):&#160;haar.c']]],
  ['sauvefeuillebinaire',['sauveFeuilleBinaire',['../compression_8c.html#aa332d4ab2688d298eeabed801e5e404b',1,'sauveFeuilleBinaire(FILE *f, noeud *n):&#160;compression.c'],['../compression_8h.html#aa332d4ab2688d298eeabed801e5e404b',1,'sauveFeuilleBinaire(FILE *f, noeud *n):&#160;compression.c']]],
  ['sauvefeuilleng',['sauveFeuilleNG',['../compression_8c.html#a502bad55e477220acc3d45c3a3a86e02',1,'sauveFeuilleNG(FILE *f, noeudHaar *n):&#160;compression.c'],['../compression_8h.html#a502bad55e477220acc3d45c3a3a86e02',1,'sauveFeuilleNG(FILE *f, noeudHaar *n):&#160;compression.c']]],
  ['sauvegardeimage',['sauvegardeImage',['../image_8c.html#a8034de76ecf7a2c222aa411191bedd02',1,'sauvegardeImage(DonneesImageRGB *img, char *path):&#160;image.c'],['../image_8h.html#a8034de76ecf7a2c222aa411191bedd02',1,'sauvegardeImage(DonneesImageRGB *img, char *path):&#160;image.c']]],
  ['se',['SE',['../structnoeud.html#a9719268a5a72eaf5d68b1687ed0f1f00',1,'noeud']]],
  ['setnoeudhaarfilsnuls',['setNoeudHaarFilsNuls',['../haar_8c.html#a2bd4992a9749e3949c912b01344f291c',1,'setNoeudHaarFilsNuls(noeudHaar *racine):&#160;haar.c'],['../haar_8h.html#a2bd4992a9749e3949c912b01344f291c',1,'setNoeudHaarFilsNuls(noeudHaar *racine):&#160;haar.c']]],
  ['seuillageng',['seuillageNG',['../transformation_8c.html#abde8c76f01c3687e9b19771455e13be6',1,'seuillageNG(matGrise *src, matGrise **new, int seuil):&#160;transformation.c'],['../transformation_8h.html#abde8c76f01c3687e9b19771455e13be6',1,'seuillageNG(matGrise *src, matGrise **new, int seuil):&#160;transformation.c']]],
  ['sizematrice',['sizeMatrice',['../structdata_feuille_haar.html#aa38ce68546570e7467a33af96174b1e1',1,'dataFeuilleHaar']]],
  ['so',['SO',['../structnoeud.html#a3a5887eb38aeeee55022a3bd6b461863',1,'noeud']]],
  ['structure_2eh',['structure.h',['../structure_8h.html',1,'']]]
];
