var searchData=
[
  ['m',['m',['../structnoeud_haar.html#a819f704c209e8fe36536189232293729',1,'noeudHaar']]],
  ['main',['main',['../main_8c.html#a3c04138a5bfe5d72780bb7e82a18e627',1,'main.c']]],
  ['main_2ec',['main.c',['../main_8c.html',1,'']]],
  ['matgrise',['matGrise',['../structmat_grise.html',1,'matGrise'],['../structure_8h.html#ab6439ab16d59810d64d1e9ae377b2317',1,'matGrise():&#160;structure.h']]],
  ['matgriseto3mat',['matGriseTo3Mat',['../matrice_8c.html#aebc81cea746bc0602a00a6e3b7359458',1,'matGriseTo3Mat(matGrise *source, matImage **dest):&#160;matrice.c'],['../matrice_8h.html#aebc81cea746bc0602a00a6e3b7359458',1,'matGriseTo3Mat(matGrise *source, matImage **dest):&#160;matrice.c']]],
  ['matimage',['matImage',['../structmat_image.html',1,'matImage'],['../structure_8h.html#a3b197cb6bdf5b0a0ae539d3feaea4247',1,'matImage():&#160;structure.h']]],
  ['matrice_2ec',['matrice.c',['../matrice_8c.html',1,'']]],
  ['matrice_2eh',['matrice.h',['../matrice_8h.html',1,'']]],
  ['menu_2ec',['menu.c',['../menu_8c.html',1,'']]],
  ['menu_2eh',['menu.h',['../menu_8h.html',1,'']]],
  ['menuautre',['menuAutre',['../menu_8c.html#a9c8f5d1f995864685325be6ca535c3d7',1,'menuAutre(matImage **m_source, matImage **m_dest, matGrise **mg):&#160;menu.c'],['../menu_8h.html#a9c8f5d1f995864685325be6ca535c3d7',1,'menuAutre(matImage **m_source, matImage **m_dest, matGrise **mg):&#160;menu.c']]],
  ['menuchargement',['menuChargement',['../menu_8c.html#ad2cca32e337d5867d90bb3050c8f3696',1,'menuChargement(matImage **m_source):&#160;menu.c'],['../menu_8h.html#ad2cca32e337d5867d90bb3050c8f3696',1,'menuChargement(matImage **m_source):&#160;menu.c']]],
  ['menucompression',['menuCompression',['../menu_8c.html#ade8206928d370c1de6c92fd0121d336b',1,'menuCompression(matImage *m_source, matImage *m_dest, matGrise *mg):&#160;menu.c'],['../menu_8h.html#ade8206928d370c1de6c92fd0121d336b',1,'menuCompression(matImage *m_source, matImage *m_dest, matGrise *mg):&#160;menu.c']]],
  ['menudecomprecomp',['menuDecompRecomp',['../menu_8c.html#a6ef66f507c0cf5fc83a0ea06954b915b',1,'menuDecompRecomp(matImage **m_source, matImage **m_dest, matGrise **mg, noeudHaar **racine):&#160;menu.c'],['../menu_8h.html#a6ef66f507c0cf5fc83a0ea06954b915b',1,'menuDecompRecomp(matImage **m_source, matImage **m_dest, matGrise **mg, noeudHaar **racine):&#160;menu.c']]],
  ['menudecompression',['menuDecompression',['../menu_8c.html#ae0565e607e55c8dd087072295a1036a4',1,'menuDecompression(matImage **m_source, matImage **m_dest, matGrise **mg):&#160;menu.c'],['../menu_8h.html#ae0565e607e55c8dd087072295a1036a4',1,'menuDecompression(matImage **m_source, matImage **m_dest, matGrise **mg):&#160;menu.c']]],
  ['menuprincipal',['menuPrincipal',['../menu_8c.html#ab1c42754eef9f47511b300a0916a6124',1,'menuPrincipal(int *choix, matImage **m_source, matImage **m_dest, matGrise **mg, noeudHaar **n):&#160;menu.c'],['../menu_8h.html#ab1c42754eef9f47511b300a0916a6124',1,'menuPrincipal(int *choix, matImage **m_source, matImage **m_dest, matGrise **mg, noeudHaar **n):&#160;menu.c']]],
  ['menusauvegarde',['menuSauvegarde',['../menu_8c.html#a5d793dfaf81d17995ed268f90f11607d',1,'menuSauvegarde(matImage *m_source, matImage *m_dest, matGrise *mg):&#160;menu.c'],['../menu_8h.html#a5d793dfaf81d17995ed268f90f11607d',1,'menuSauvegarde(matImage *m_source, matImage *m_dest, matGrise *mg):&#160;menu.c']]],
  ['menutransformation',['menuTransformation',['../menu_8c.html#ab250d4de83796fc07537219d7bdc444e',1,'menuTransformation(matImage *m_source, matImage **m_dest, matGrise **mg):&#160;menu.c'],['../menu_8h.html#ab250d4de83796fc07537219d7bdc444e',1,'menuTransformation(matImage *m_source, matImage **m_dest, matGrise **mg):&#160;menu.c']]],
  ['mm',['mm',['../structnoeud_haar.html#aaba1695bd3a1e7b92aee676340b5969a',1,'noeudHaar']]],
  ['mp',['mp',['../structnoeud_haar.html#ab18773f897405fc6cb42a9e0208ea0c4',1,'noeudHaar']]]
];
