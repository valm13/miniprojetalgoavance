var searchData=
[
  ['affiche3mat',['affiche3Mat',['../matrice_8c.html#a6126054b6951b091979c9c9106084b72',1,'affiche3Mat(matImage *m):&#160;matrice.c'],['../matrice_8h.html#a6126054b6951b091979c9c9106084b72',1,'affiche3Mat(matImage *m):&#160;matrice.c']]],
  ['affichepixelsimage',['affichePixelsImage',['../image_8c.html#a9fe98933b0ffdc15a9123ba68e622b67',1,'affichePixelsImage(DonneesImageRGB *img):&#160;image.c'],['../image_8h.html#a9fe98933b0ffdc15a9123ba68e622b67',1,'affichePixelsImage(DonneesImageRGB *img):&#160;image.c']]],
  ['afficheprefixe',['affichePrefixe',['../arbre_8c.html#a04c0ececf52f1aca2764c52c22583cfb',1,'affichePrefixe(noeud *racine):&#160;arbre.c'],['../arbre_8h.html#a04c0ececf52f1aca2764c52c22583cfb',1,'affichePrefixe(noeud *racine):&#160;arbre.c']]],
  ['affichetableau2dentre',['afficheTableau2DEntre',['../arbre_8c.html#a3ff44e1a6811710f6236a99b52ed36f5',1,'afficheTableau2DEntre(short int **t, int xmin, int xmax, int ymin, int ymax):&#160;arbre.c'],['../arbre_8h.html#a3ff44e1a6811710f6236a99b52ed36f5',1,'afficheTableau2DEntre(short int **t, int xmin, int xmax, int ymin, int ymax):&#160;arbre.c']]],
  ['alloue3matrice',['alloue3Matrice',['../matrice_8c.html#a670d75f4d8af168950c5cf06a7a2c7c9',1,'alloue3Matrice(int n, int m, matImage **new):&#160;matrice.c'],['../matrice_8h.html#a670d75f4d8af168950c5cf06a7a2c7c9',1,'alloue3Matrice(int n, int m, matImage **new):&#160;matrice.c']]],
  ['allouematricegrise',['alloueMatriceGrise',['../matrice_8c.html#a825e0a2f2ca42777abbb9f4115906ee4',1,'alloueMatriceGrise(int n, int m, matGrise **new):&#160;matrice.c'],['../matrice_8h.html#a825e0a2f2ca42777abbb9f4115906ee4',1,'alloueMatriceGrise(int n, int m, matGrise **new):&#160;matrice.c']]]
];
