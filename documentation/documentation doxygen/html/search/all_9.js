var searchData=
[
  ['l',['l',['../structdata_feuille.html#a3955a84e14192645a5b07aa34f7b1e0e',1,'dataFeuille']]],
  ['largeur',['largeur',['../structen_tete.html#a0f73cc7930502972878409b8fe2ecfca',1,'enTete']]],
  ['libere3matrices',['libere3Matrices',['../matrice_8c.html#a911eafad9049a6986744207a0d3ad283',1,'libere3Matrices(matImage **m):&#160;matrice.c'],['../matrice_8h.html#a911eafad9049a6986744207a0d3ad283',1,'libere3Matrices(matImage **m):&#160;matrice.c']]],
  ['liberearbre',['libereArbre',['../arbre_8c.html#aa8b1c448e461a3abfb4df60374e08e45',1,'libereArbre(noeud **racine):&#160;arbre.c'],['../arbre_8h.html#aa8b1c448e461a3abfb4df60374e08e45',1,'libereArbre(noeud **racine):&#160;arbre.c']]],
  ['liberearbrehaar',['libereArbreHaar',['../haar_8c.html#a07c21f5a97079bbc6fc3caba547bbad1',1,'libereArbreHaar(noeudHaar **racine):&#160;haar.c'],['../haar_8h.html#a07c21f5a97079bbc6fc3caba547bbad1',1,'libereArbreHaar(noeudHaar **racine):&#160;haar.c']]],
  ['liberematrice',['libereMatrice',['../matrice_8c.html#adbafcbb41301101004f4cb83f1d8ab7e',1,'libereMatrice(int nblig, short int **t):&#160;matrice.c'],['../matrice_8h.html#adbafcbb41301101004f4cb83f1d8ab7e',1,'libereMatrice(int nblig, short int **t):&#160;matrice.c']]],
  ['liberematricegrise',['libereMatriceGrise',['../matrice_8c.html#a707960a9237dcafefbb5ad59604f41fd',1,'libereMatriceGrise(matGrise **mat):&#160;matrice.c'],['../matrice_8h.html#a707960a9237dcafefbb5ad59604f41fd',1,'libereMatriceGrise(matGrise **mat):&#160;matrice.c']]],
  ['listefichiersfrom',['listeFichiersFrom',['../menu_8c.html#a972f938dc05444b237dd7cbfbd11de07',1,'listeFichiersFrom(char *path):&#160;menu.c'],['../menu_8h.html#a972f938dc05444b237dd7cbfbd11de07',1,'listeFichiersFrom(char *path):&#160;menu.c']]]
];
