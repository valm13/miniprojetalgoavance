var indexSectionsWithContent =
{
  0: "abcdefghilmnprstvxy",
  1: "cdemnp",
  2: "achimst",
  3: "abcdeflmnprsv",
  4: "beghlmnprstvxy",
  5: "cdemnp"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs"
};

