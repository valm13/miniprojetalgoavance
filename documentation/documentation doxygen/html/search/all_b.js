var searchData=
[
  ['nbbitsparpixels',['nbBitsParPixels',['../structen_tete.html#ad9c55013e1fd30b3e792ac3579d4c0b8',1,'enTete']]],
  ['nbcol',['nbcol',['../structmat_image.html#af0dc40852693dafaf2d7a6f18b17574c',1,'matImage::nbcol()'],['../structmat_grise.html#af0dc40852693dafaf2d7a6f18b17574c',1,'matGrise::nbcol()']]],
  ['nblig',['nblig',['../structmat_image.html#a039ba0592f66db5d933fda2113f1f64c',1,'matImage::nblig()'],['../structmat_grise.html#a039ba0592f66db5d933fda2113f1f64c',1,'matGrise::nblig()']]],
  ['ne',['NE',['../structnoeud.html#ad611e22a98fb72b0d9b66852b2c082e2',1,'noeud']]],
  ['negatifimage',['negatifImage',['../transformation_8c.html#abb21025b8b441698b3a4c7b15e58d5c6',1,'negatifImage(matImage *src, matImage **new):&#160;transformation.c'],['../transformation_8h.html#abb21025b8b441698b3a4c7b15e58d5c6',1,'negatifImage(matImage *src, matImage **new):&#160;transformation.c']]],
  ['no',['NO',['../structnoeud.html#a1e93ffececa7921fd5056f8a2b1faaaf',1,'noeud']]],
  ['noeud',['noeud',['../structnoeud.html',1,'noeud'],['../structure_8h.html#a23a591b87421332275c57bbc0bd88c83',1,'noeud():&#160;structure.h']]],
  ['noeudhaar',['noeudHaar',['../structnoeud_haar.html',1,'noeudHaar'],['../structure_8h.html#a341329d26b9331c5f620daded9984a6d',1,'noeudHaar():&#160;structure.h']]]
];
