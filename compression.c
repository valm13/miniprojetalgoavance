#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libISEN/BmpLib.h"
#include "structure.h"
#include "matrice.h"
#include "haar.h"
#include "compression.h"

void compressionBinaireSP(char *chemin, noeud *racine)
{
	if(racine != NULL)
	{
		FILE * f;
		
		// Ouverture du fichier
		f = fopen(chemin , "wb");

		// Données de l'en-tête
		enTete header;
		header.nbBitsParPixels = 1;
		header.typeCompression = 0;
		// Le + 1 pour avoir la taille et non la derniere case du tableau
		header.largeur = racine->pos.xmax - racine->pos.xmin + 1; 
		header.hauteur = racine->pos.ymax - racine->pos.ymin + 1;

		// Ecriture de l'en-tête dans le fichier
		fwrite(&header,sizeof(enTete),1,f);

		// On sauvegarde chaques feuilles dans le fichier
		sauveFeuilleBinaire(f,racine);

		fclose(f);
	}
}

void sauveFeuilleBinaire(FILE *f, noeud *n)
{
	if(n != NULL)
	{
		if(n->val == 2)
		{
			// printf("noeud\n");
			sauveFeuilleBinaire(f,n->NO);
			sauveFeuilleBinaire(f,n->NE);
			sauveFeuilleBinaire(f,n->SO);
			sauveFeuilleBinaire(f,n->SE);
		}
		else
		{
			// printf("feuille\n");
			// X0 --> Y0 --> l (négatif si noir(n->val == 0), positif si blanc (n->val == 1))
			dataFeuille data;

			data.x0 = n->pos.xmin;
			data.y0 = n->pos.ymin;
			data.l = (n->val == 0 ?-(n->pos.xmax - n->pos.xmin) :(n->pos.xmax - n->pos.xmin));

			fwrite(&data,sizeof(dataFeuille),1,f);
		}
	}
	
}

int decompressionBinaireSP(char *nomFichier, matGrise **m)
{
	FILE * f;
	chaine c;
	matGrise *new = NULL;
	dataFeuille data;

	// Chemin + ouverture du fichier
	strcpy(c,"binaires/image_binaire/");
	strcat(c,nomFichier);
	strcat(c,".bin");
	f = fopen(c , "rb");

	if(f != NULL)
	{
		// Données de l'en-tête
		enTete header;
		fread(&header,sizeof(header),1,f);
		if(header.nbBitsParPixels == 1 && header.typeCompression == 0)
		{
			// Allocation de la matrice
			alloueMatriceGrise(header.hauteur, header.largeur, &new);

			// Tant qu'on lis des données on remplis les carrés associés
			int z = 1;
			while(fread(&data,sizeof(dataFeuille),1,f))
			{
				// printf("%d\n",z );
				// printf("data.l = %d\n",data.l );
				// coté = valeur absolue de l
				int cote = (data.l < 0)?-data.l:data.l;

				for(int i = data.y0; i <= data.y0 + cote; i++)
				{
					for(int j = data.x0; j <= data.x0 + cote; j++)
					{
						new->g[i][j] = (data.l < 0)?0:255;
					}
				}
				z++;
			}
			*m = new;
		}
		fclose(f);
		return 1;
	}
	else
		return 0;
}

void compressionNG_SP(char *nomFichier, matGrise *m)
{
	
	if(m != NULL)
	{
		noeudHaar *racine;
		FILE * f;
		chaine c;
			
		// Chemin + ouverture du fichier
		strcpy(c,"binaires/image_ng");
		strcat(c,nomFichier);
		strcat(c,".bin");
		f = fopen(c , "wb");

		// Données de l'en-tête
		enTete header;
		header.nbBitsParPixels = 8;
		header.typeCompression = 0;
		header.largeur = m->nbcol; 
		header.hauteur = m->nblig;

		// Ecriture de l'en-tête dans le fichier
		fwrite(&header,sizeof(enTete),1,f);
		
		// On crée l'arbre
		decomposeNNiveaux(m, &racine,-1);


		// On sauvegarde chaques feuilles dans le fichier
		sauveFeuilleNG(f,racine);

		// On libere l'arbre
		libereArbreHaar(&racine);


		fclose(f);		
	}
}

void sauveFeuilleNG(FILE *f, noeudHaar *n)
{
	if(n != NULL)
	{
		dataFeuilleHaar data;
		int *compressedArray = NULL;
		int *array = NULL;

		if(n->pp == NULL)
		{
			// On est tout en bas, on traite le dernier pp qui a forcement une matrice m que l'on sauvegarde
			if(n->m != NULL)
			{
				// Sauvegarde de PP
				// printf("feuille pp\n");

				parcoursDiagonale(n->m, &array);

				data.typeFeuille = 0;
				// On compresse le tableau array et récupère sa taille
				data.sizeMatrice = compresseTableau(array, n->m->nblig*n->m->nbcol, &compressedArray);
				fwrite(&data,sizeof(dataFeuilleHaar),1,f);
				fwrite(compressedArray,sizeof(short int),data.sizeMatrice,f);

				// Puis on libère les tableaux alloués
				free(array);
				free(compressedArray);
			}
			else
				printf("Probleme feuille pp (matrice == NULL)\n");
			
		}
		else
		{
			// On est sur un étage de l'arbre
			// Il faut faire un appel récursif du fils pp (vu qu'il existe)
			// Et Ensuite sauvegarder pm,mp,mm dans le fichier (car on fait en postfixe)

			// Recursion
			sauveFeuilleNG(f,n->pp);

			if(n->pm->m != NULL)
			{
				// Sauvegarde de PM
				// printf("feuille pm\n");

				parcoursDiagonale(n->pm->m, &array);

				data.typeFeuille = 1;
				// On compresse le tableau array et récupère sa taille
				data.sizeMatrice = compresseTableau(array, n->pm->m->nblig*n->pm->m->nbcol, &compressedArray);
				fwrite(&data,sizeof(dataFeuilleHaar),1,f);
				fwrite(compressedArray,sizeof(short int),data.sizeMatrice,f);
				
				// Puis on libère les tableaux alloués
				free(array);
				free(compressedArray);
			}
			else
				printf("Probleme feuille pm (matrice == NULL)\n");

			if(n->mp->m != NULL)
			{
				// Sauvegarde de PM
				// printf("feuille mp\n");

				parcoursDiagonale(n->mp->m, &array);

				data.typeFeuille = 1;
				// On compresse le tableau array et récupère sa taille
				data.sizeMatrice = compresseTableau(array, n->mp->m->nblig*n->mp->m->nbcol, &compressedArray);
				fwrite(&data,sizeof(dataFeuilleHaar),1,f);
				fwrite(compressedArray,sizeof(short int),data.sizeMatrice,f);
				
				// Puis on libère les tableaux alloués
				free(array);
				free(compressedArray);
			}
			else
				printf("Probleme feuille mp (matrice == NULL)\n");

			if(n->mm->m != NULL)
			{
				// Sauvegarde de PM
				// printf("feuille mm\n");

				parcoursDiagonale(n->mm->m, &array);

				data.typeFeuille = 1;
				// On compresse le tableau array et récupère sa taille
				data.sizeMatrice = compresseTableau(array, n->mm->m->nblig*n->mm->m->nbcol, &compressedArray);
				fwrite(&data,sizeof(dataFeuilleHaar),1,f);
				fwrite(compressedArray,sizeof(short int),data.sizeMatrice,f);
				
				// Puis on libère les tableaux alloués
				free(array);
				free(compressedArray);
			}
			else
				printf("Probleme feuille mm (matrice == NULL)\n");
			
		}
	}
	
}

void decompressionBinaireNG(char *nomFichier, matGrise **m)
{
	FILE * f;
	chaine c;
	matGrise *new = NULL;
	dataFeuilleHaar data;

	// Chemin + ouverture du fichier
	strcpy(c,"fichiersBinaire/");
	strcat(c,nomFichier);
	strcat(c,".bin");
	f = fopen(c , "rb");

	if(f != NULL)
	{
		// Données de l'en-tête
		enTete header;
		fread(&header,sizeof(header),1,f);
		if(header.nbBitsParPixels == 8 && header.typeCompression == 0)
		{
			// Allocation de la matrice
			alloueMatriceGrise(header.hauteur, header.largeur, &new);

			// Tant qu'on lis des données on remplis les carrés associés
			int *temp;
			while(fread(&data,sizeof(dataFeuilleHaar),1,f))
			{
				/*switch(data.typeFeuille){
					case 0:printf("pp");break;
					case 1:printf("pm");break;
					case 2:printf("mp");break;
					case 3:printf("mm");break;
					default: printf("je sais pas ce qu'est cette feuille\n");break;
				}
				printf(": size = %d\n",data.sizeMatrice);
*/
				temp = (int *)malloc(data.sizeMatrice*sizeof(int));
				
				fread(temp,sizeof(int),data.sizeMatrice,f);
				free(temp);
				temp = NULL;		
			}
			// *m = new;
		}
	}
	fclose(f);
}

void parcoursDiagonale(matGrise *m, int **Res)
{
	const int MaxX = m->nbcol - 1;
	const int MaxY = m->nblig - 1;
	const int nbCases = m->nbcol * m->nblig;

	pos ptr;
	// On initialise notre pointeur en haut à gauche
	ptr.x = 0;
	ptr.y = MaxY;

	int *T = (int *)malloc(nbCases*sizeof(int));
	for (int i = 0; i < nbCases; ++i)
	{
		T[i] = -1;
	}
	int cpt = 0;
	// printf("Il y a %d cases \n",nbCases );
	
	// Parcours de l'image
	while(cpt != nbCases)
	{
		// On rentre la valeur dans le tableau à la position ptr de l'image
		if(ptr.x <= MaxX && ptr.y <= MaxY && ptr.x >= 0 && ptr.y >= 0)
		{
			T[cpt] = m->g[ptr.y][ptr.x];
			cpt++;	
		}
	

		if(ptr.y == MaxY)
		{
			if(estPaire(ptr.x))
			{	
				// On décale le pointeur à la case de droite
				ptr.x++;
			}
			else
			{
				// Diagonale Bas (je stocke la diagonale sauf le pixel en x = 0)
				do
				{
					// Changement de la position de ptr
					ptr.x--;
					ptr.y--;

					T[cpt] = m->g[ptr.y][ptr.x];
					cpt++;
				}while(ptr.x != 0);
				cpt--;
				
			}
		}
		
		else if(ptr.x == 0)
		{
			if(ptr.y == 0) // Coin bas gauche
			{
				ptr.x++;
			}
			else if(estPaire(ptr.y))
			{
				// On décale le pointeur à la case de droite
				ptr.y--;
			}
			else
			{
				// Diagonale Haut (stocke la diagonale sauf le pixel en y = MaxY)
				do
				{
					// Changement de la position de ptr
					ptr.x++;
					ptr.y++;

					T[cpt] = m->g[ptr.y][ptr.x];
					cpt++;
					
				}while(ptr.y != MaxY);
				cpt--;
				
			}
		}
		
		else if(ptr.x == MaxX)
		{
			if(estPaire(ptr.y))
			{
				// On décale le pointeur à la case de droite
				ptr.y--;
			}
			else
			{
				// Diagonale Haut (stocke la diagonale sauf le pixel en y = MaxY)
				do
				{
					// Changement de la position de ptr
					ptr.x--;
					ptr.y--;

					T[cpt] = m->g[ptr.y][ptr.x];
					cpt++;
					
				}while(ptr.y != 0);
				cpt--;
				
			}
		}
		else if(ptr.y == 0)
		{
		/*	if(ptr.x == 0)
				ptr.x++;*/
			if(estPaire(ptr.x))
			{
				// On décale le pointeur à la case de droite
				ptr.x++;
			}
			else
			{
				// Diagonale Haut (stocke la diagonale sauf le pixel en y = MaxY)
				do
				{
					// Changement de la position de ptr
					ptr.x++;
					ptr.y++;

					T[cpt] = m->g[ptr.y][ptr.x];
					cpt++;
					
				}while(ptr.x != MaxX);
				cpt--;
				
			}
		}
	}





	*Res = T;
}

int estPaire(int x)
{
	if(x % 2 == 0)
		return 1;
	else
		return 0;
}

int compteNombreDifferents(int size, int *T)
{
	int cpt = 1;
	int last = T[0];
	for (int i = 1; i < size; i++)
	{
		if(last != T[i])
		{
			cpt++;
			last = T[i];
		}
	}
	return cpt;
}

void remplisSousTabNombre(int sizeBase, int sizeSousTab, int *TBase, int *TNombre)
{
	int cpt = 0;
	TNombre[0] = TBase[0];
	int last = TNombre[0];

	for (int i = 1; i < sizeBase; i++)
	{
		if(last != TBase[i])
		{
			cpt++;
			TNombre[cpt] = TBase[i];
			last = TBase[i];
		}
	}
}

void remplisSousTabIteration(int sizeBase, int sizeSousTab, int *TBase, int *TIteration, int *TNombre)
{
	int ptr = 0;

	for(int i = 0; i < sizeSousTab; i++)
	{
		TIteration[i] = 0;
		do{
			TIteration[i]++;
			ptr++;
			// printf("i = %d, ptr = %d, sizebase = %d,sizeSousTab = %d\n",i,ptr,sizeBase,sizeSousTab );
		}while(ptr < sizeBase && TNombre[i] == TBase[ptr]);
	}
}

int compresseTableau(int *TBase, int sizeBase, int **TCompresse)
{
	int sizeSousTab = compteNombreDifferents(sizeBase, TBase);
	int *sousTabNombre = (int *)malloc(sizeSousTab*sizeof(int));
	int *sousTabIteration = (int *)malloc(sizeSousTab*sizeof(int));

	remplisSousTabNombre(sizeBase,sizeSousTab,TBase,sousTabNombre);
	remplisSousTabIteration(sizeBase,sizeSousTab,TBase,sousTabIteration,sousTabNombre);
	// Ca marche(j'ai bien mes deux tableaux, 1 avec les nombres, l'autre avec les iterations pour chaque nombre)

	/*for (int i = 0; i < sizeSousTab; ++i)
	{
		printf("TNombre[%d] = %d | TIteration[%d] = %d\n",i,sousTabNombre[i],i, sousTabIteration[i]);
	}*/

	
	int taille = evalueTailleTableauCompresse(sizeSousTab,sousTabIteration);
	int *resultat = (int *)malloc(taille*sizeof(int));

	remplisTableauCompresse(taille,sizeSousTab,sousTabNombre,sousTabIteration,resultat);

	free(sousTabIteration);
	free(sousTabNombre);
	*TCompresse = resultat;

	return taille;
}

int evalueTailleTableauCompresse(int size,int *TIteration)
{
	int cpt = 0;

	for(int i = 0; i < size; i++)
	{
		if(TIteration[i] == 1)
			cpt++;
		else if(TIteration[i] > 1)
			cpt += 2;
	}

	return cpt;
}

void remplisTableauCompresse(int sizeCompresse, int sizeSousTab, int *TNombre, int *TIteration, int *TCompresse)
{
	int ptr = 0;
	for(int i = 0; i < sizeSousTab; i++)
	{
		if(TIteration[i] > 1)
		{
			TCompresse[ptr] = -TIteration[i];
			ptr++;
			
		}
		TCompresse[ptr] = TNombre[i];
		ptr++;
	}
}