#ifndef COMP_H
#define COMP_H
/**
 * @brief Compresse une image binaire à partir de l'arbre quaternaire associé
 * @details Sauvegarde le fichier dans le dossier binaires/image_binaire/
 * 
 * @param chemin Chemin du fichier à créer
 * @param racine Pointeur sur la racine de l'arbre quaternaire
 */
void compressionBinaireSP(char *chemin, noeud *racine);

/**
 * @brief Sauvegarde chaque feuille de l'arbre dans le fichier en paramètre
 * 
 * @param f Fichier ouvert
 * @param n Noeud de l'arbre
 */
void sauveFeuilleBinaire(FILE *f, noeud *n);

/**
 * @brief Décompresse le fichier binaire dans la matrice grise
 * 
 * @param nomFichier Nom du fichier à décompresser
 * @param m Adresse du pointeur sur matGrise
 * 
 * @return 1 si l'opération a fonctionné, 0 sinon
 */
int decompressionBinaireSP(char *nomFichier, matGrise **m);

/**
 * @brief Compresse une image en niveau de gris
 * @details NOT WORKING
 * 
 * @param nomFichier nom du fichier binaire à créer
 * @param m Pointeur sur la matrice à compresser
 */
void compressionNG_SP(char *nomFichier, matGrise *m);

/**
 * @brief Sauvegarde les feuilles de l'arbre Haar
 * @details NOT WORKING
 * 
 * @param f Fichier ouvert
 * @param n Noeud de l'arbre
 */
void sauveFeuilleNG(FILE *f, noeudHaar *n);

/**
 * @brief Décompresse un fichier binaire en niveau de gris
 * @details NOT WORKING
 * 
 * @param nomFichier nom du fichier à décompresser
 * @param m Adresse du pointeur de la matrice dans laquelle décompresser le fichier
 */
void decompressionBinaireNG(char *nomFichier, matGrise **m);

/**
 * @brief Vérifie si le nombre est pair
 * 
 * @param x Entier
 * @return 1 si pair, 0 sinon
 */
int estPaire(int x);

/**
 * @brief Parcours une matrice en diagonale et rentre les valeurs dans un tableau 1D
 * @details WORKING
 * 
 * @param m Pointeur sur la matrice à parcourir
 * @param Res Adresse d'un pointeur sur un entier
 */
void parcoursDiagonale(matGrise *m, int **Res);

/**
 * @brief Compte le nombre de nombres different sans les répétitions
 * 
 * @param size Taille du tableau
 * @param T Tableau
 * 
 * @return Nombre de nombre différents dans l'ordre
 */
int compteNombreDifferents(int size, int *T);

/**
 * @brief Remplis un sous tableau avec les nombres dans l'ordre en enlevant les répetitions successives
 * 
 * @param sizeBase Taille du tableau de base
 * @param sizeSousTab Taille du sous tableau
 * @param TBase Tableau de base
 * @param TNombre Sous tableau des nombres
 */
void remplisSousTabNombre(int sizeBase, int sizeSousTab, int *TBase, int *TNombre);

/**
 * @brief Remplis le sous tableau TIteration avec le nombre d'itération correspondant à chaqun des nombre du tableau TNombre
 * 
 * @param sizeBase Taille du tableau de base
 * @param sizeSousTab Taille du sous tableau
 * @param TBase Tableau de base
 * @param TIteration sous tableau des itérations
 * @param TNombre sous tableau des nombres
 */
void remplisSousTabIteration(int sizeBase, int sizeSousTab, int *TBase, int *TIteration, int *TNombre);

/**
 * @brief Compresse le tableau TBase
 * @details Créer un tableau qui contient les répétitions de nombres dans des nombres négatif précédant la valeur répétée
 * 
 * @param TBase Tableau de base
 * @param sizeBase Taille du tableau de base
 * @param TCompresse Tableau compressé
 * @return Renvois la taille du tableau compressé
 */
int compresseTableau(int *TBase, int sizeBase, int **TCompresse);

/**
 * @brief Evalue la taille du tableau compressé
 * 
 * @param size Taille du tableau d'itérations
 * @param TIteration Tableau d'itérations
 * 
 * @return Renvois la taille du tableau compressé
 */
int evalueTailleTableauCompresse(int size,int *TIteration);

/**
 * @brief Rempli le tableau compressé à partir des 2 sous tableaux TNombre et TIteration
 * @details WORKING
 * 
 * @param sizeCompresse Taille du tableau compressé
 * @param sizeSousTab taille des sous tableaux
 * @param TNombre Sous tableau des nombres
 * @param TIteration Sous tableau des itérations
 * @param TCompresse Tableau compressé
 */
void remplisTableauCompresse(int sizeCompresse, int sizeSousTab, int *TNombre, int *TIteration, int *TCompresse);
#endif