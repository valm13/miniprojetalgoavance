#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include "libISEN/BmpLib.h"
#include "structure.h"
#include "matrice.h"
#include "image.h"
#include "transformation.h"
#include "arbre.h"
#include "haar.h"
#include "compression.h"
#include "menu.h"


int main(int argc, char **argv)
{
	int choix;

	matImage *mSource = NULL;
	matImage *mDest = NULL;
	matGrise *mg = NULL;
	noeudHaar *n = NULL;

	do
	{
		menuPrincipal(&choix, &mSource, &mDest, &mg, &n);
	}while(choix != 8);

	libere3Matrices(&mSource);
	libere3Matrices(&mDest);
	libereMatriceGrise(&mg);

	return 0;
}