#ifndef IMG_H
#define IMG_H
/**
 * @brief Sauvegarde une image au chemin indiqué
 * 
 * @param img Pointeur sur l'image
 * @param path Chemin
 */
void sauvegardeImage(DonneesImageRGB *img, char *path);

/**
 * @brief Enleve l'extension du chemin d'une image bmpRemove the extension of the path for any image bmp
 * 
 * @param path Chemin de l'image
 * @return Renvoi 1 si l'opération a fonctionné, 0 sinon
 */
int enleveExtension(char *path);

/**
 * @brief Crée une copie de l'image à partir du chemin de celle ci
 * @details Ajoute "- Copie" à la fin du nom de l'image copiée
 * 
 * @param path Tableau de caractères
 */
void copieImage(char *path);

/**
 * @brief Affiche les pixels d'une image
 * 
 * @param img Pointeur sur une image
 */
void affichePixelsImage(DonneesImageRGB *img);

/**
 * @brief Cree une image à partir de 3 Tableau 2D de dimensions (n,m)
 * 
 * @param n Hauteur
 * @param m Largeur
 * @param int Tableau 2D de rouge
 * @param int Tableau 2D de vert
 * @param int Tableau 2D de bleu
 * @param img Double pointeur sur une image
 */
void creeImage(int n, int m, short int **r, short int **v, short int **b, DonneesImageRGB **img);

/**
 * @brief Cree une image à partir d'une matImage
 * 
 * @param m Pointeur sur matImage
 * @param img Double Pointeur sur une Image
 */
void creeImageFromMatImage(matImage *mat, DonneesImageRGB **img);

/**
 * @brief Cree une image à partir d'une matGrise
 * 
 * @param m Pointeur sur matGrise
 * @param img Double Pointeur sur une Image
 */
void creeImageFromMatGrise(matGrise *mat, DonneesImageRGB **img);
#endif