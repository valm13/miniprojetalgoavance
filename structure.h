#ifndef STRUCT_H
#define STRUCT_H
/**
 * Chaine est une chaine de caractères de 50 cases dont le '\0'
 */
typedef char chaine[50];

/**
 * @brief Matrice colorée
 * @details Contient 3 Matrices R,V et B ainsi que leurs dimensions. Permet de stocker des pixels colorés
 * 
 */
typedef struct matImage{
	int nblig, nbcol;
	short int **r, **v, **b;
}matImage;

/**
 * @brief Matrice grise
 * @details Contient 1 Matrices g ainsi que ses dimensions. Permet de stocker des pixels gris
 * 
 */
typedef struct matGrise{
	int nblig, nbcol;
	short int **g;
}matGrise;

/**
 * @brief Coordonnées de 2 points
 * 
 */
typedef struct coord{
	int xmin,xmax,ymin,ymax;
}coord;

/**
 * @brief Noeud utilisé dans la partie 3
 * 
 */
typedef struct noeud{
	int val,etic;
	struct noeud *NO,*NE,*SO,*SE;
	coord pos;
}noeud;

/**
 * @brief C'est une position
 * 
 */
typedef struct pos{
	int x,y;
}pos;

/**
 * @brief Noeud Haar utilisé dans la partie 4
 * @details Chaque feuille contient une matrice
 * 
 */
typedef struct noeudHaar{
	matGrise *m;
	struct noeudHaar *pp,*pm,*mp,*mm;
}noeudHaar;

/**
 * @brief En-tête des fichiers binaires
 * 
 */
typedef struct enTete{
	char nbBitsParPixels,typeCompression;
	short int hauteur,largeur;
}enTete;

/**
 * @brief Donnée d'une feuille d'un arbre quaternaire de la partie 3
 * @details Origine du carré aux coordonnées x0,y0 et l son coté; si l est positif le carré est blanc, sinon il est noir
 * 
 */
typedef struct dataFeuille{
	short int x0, y0, l;
}dataFeuille;

/**/
/**
 * @brief Donnée d'une feuille d'un arbre Haar de la partie 4
 * @details typeFeuille = 	0 <=> pp, 1 <=> pm,	2 <=> mp, 3 <=> mm
 * 
 */
typedef struct dataFeuilleHaar{
	short int typeFeuille, sizeMatrice;
}dataFeuilleHaar;
#endif