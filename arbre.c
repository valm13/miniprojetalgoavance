#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "structure.h"
#include "matrice.h"
#include "arbre.h"

int verifieCouleur(noeud *N, short int **g)
{
	int cptBlanc,cptNoir,nbPixel;
	int display = 0;
	cptNoir = cptBlanc = 0;
	nbPixel = (N->pos.xmax - N->pos.xmin) + 1; // Sur une ligne
	nbPixel = nbPixel * nbPixel; // Au total

	for(int i = N->pos.ymin; i <= N->pos.ymax; i++){
		for(int j = N->pos.xmin; j <= N->pos.xmax; j++){
			if(g[i][j] == 255)
				cptBlanc++;
			else if(g[i][j] == 0)
				cptNoir++;
		}
	}
	if(cptNoir == nbPixel){
		if(display)
			printf("C'est tout noir\n");
		return 0;
	}
	else if(cptBlanc == nbPixel){
		if(display)
			printf("C'est tout blanc\n");
		return 1;
	}
	else{
		if(display)
			printf("C'est ni noir ni blanc\n");
		return 2;
	}

}

void creeArbreQuaternaire(matGrise *m, noeud **racine, int xmin, int xmax, int ymin, int ymax)
{
	if(m != NULL)
	{
		// printf("xmin=%d,xmax=%d,ymin=%d,ymax=%d\n",xmin,xmax,ymin,ymax );
		//	Alloc
		noeud *N = (noeud*)malloc(sizeof(noeud));

		//	Pos
		N->pos.xmin = xmin;
		N->pos.xmax = xmax;
		N->pos.ymin = ymin;
		N->pos.ymax = ymax;
		N->etic = 0;
		N->NO = N->NE = N->SO = N->SE = NULL;

		N->val = verifieCouleur(N,m->g);
		// printf("toto1\n");
		//	Valeur &  ? fils ?
		if(N->val == 2)
		{
			int centreX = (int)((xmax-xmin)/2)+xmin;
			int centreY = (int)((ymax-ymin)/2)+ymin;
			// printf("toto2\n");

			creeArbreQuaternaire(m,&(N->SO),xmin, centreX, ymin, centreY); // SO
			// printf("toto3\n");

			creeArbreQuaternaire(m,&(N->SE),centreX+1, xmax  , ymin, centreY); // SE
			// printf("toto4\n");


			creeArbreQuaternaire(m,&(N->NO),xmin, centreX,centreY+1,ymax); // NO
			// printf("toto5\n");

			creeArbreQuaternaire(m,&(N->NE),centreX+1, xmax,centreY+1,ymax); // NE
			// printf("toto6\n");

		}
		*racine = N;
	}
	else
		printf("La matrice est vide\n");
}

void affichePrefixe(noeud *racine)
{
	if(racine != NULL)
	{
		printf("%d-",racine->val);
		affichePrefixe(racine->SO);
		affichePrefixe(racine->SE);
		affichePrefixe(racine->NO);
		affichePrefixe(racine->NE);
	}
}

void creeMatriceFromArbreQuaternaire(noeud *racine, matGrise **NEW, int etic)
{
	int size;
	matGrise *new = NULL;
	if(racine != NULL)
	{
		int nb = 0;
		size = racine->pos.xmax - racine->pos.xmin;
		alloueMatriceGrise(size+1,size+1,&new);
		if(etic)
			etiquetteFeuilleNB(racine, &nb);
		colorisMatrice(racine,new->g,etic);
		*NEW = new;
	}
}

void colorisMatrice(noeud *racine,short int **g,int etic)
{
	// quand c'est une feuille on coloris dans le care du noeud sinon on appelle les fils
	if(racine != NULL)
	{
		if(racine->val != 2)
		{
			colorisNoeud(racine,g,etic);
			
		}
		else
		{
			colorisMatrice(racine->SO,g,etic);
			colorisMatrice(racine->SE,g,etic);
			colorisMatrice(racine->NO,g,etic);
			colorisMatrice(racine->NE,g,etic);
		}
	}
}
void colorisNoeud(noeud *N,short int **g, int etic)
{
	for (int i = N->pos.ymin; i <= N->pos.ymax; i++)
	{
		for (int j = N->pos.xmin; j <= N->pos.xmax; j++)
		{
			if(!etic)
				g[i][j] = (N->val == 0) ? 0 : 255;
			else
				g[i][j] = N->etic;
		}
	}
}

void libereArbre(noeud **racine)
{
	noeud *n = *racine;
	if(n->val == 2)
	{
		libereArbre(&(n->SO));
		libereArbre(&(n->SE));
		libereArbre(&(n->NO));
		libereArbre(&(n->NE));
	}
	free(n);
	n = NULL;
}

void afficheTableau2DEntre(short int **t,int xmin,int xmax,int ymin,int ymax)
{
	for (int i = ymin; i < ymax; i++)
	{
		for (int j = xmin; j < xmax; j++)
		{
			printf("T[%d][%d] = %d\n",i,j,t[i][j]);
		}
	}
}


void etiquetteFeuilleNB(noeud *racine, int *nb)
{
	if(racine != NULL)
	{
		if(racine->val == 2)
		{
			racine->etic = -1;
			(*nb) += 20;
			(*nb) = (*nb) % 256;
			etiquetteFeuilleNB(racine->SO,nb);
			etiquetteFeuilleNB(racine->SE,nb);
			etiquetteFeuilleNB(racine->NO,nb);
			etiquetteFeuilleNB(racine->NE,nb);
		}
		else
		{
			racine->etic = *nb;
		}
	}
}