#include <stdlib.h>
#include <stdio.h>
#include "tri.h"

void bubbleSortAsc(int *T,int size)
{
	int temp;
	for (int c = 0 ; c < size - 1; c++)
    {
	    for (int d = 0 ; d < size - c - 1; d++)
	    {
	      if (T[d] > T[d+1]) /* For decreasing order use < */
	      {
	        temp = T[d];
	        T[d] = T[d+1];
	        T[d+1] = temp;
	      }
	    }
    }
}