#ifndef MENU_H
#define MENU_H
/**
 * @brief Menu Principal
 * @details Permet d'appeler les sous menus
 * 
 * @param choix Choix de l'utilisateur
 * @param m_source matImage source
 * @param m_dest matImage dest utilisée principalement pour faire le negatif
 * @param mg matGrise utilisée pour les autres transformations
 * @param n noeudHaar utilisé pour la décomposition et recomposition d'image
 */
void menuPrincipal(int *choix, matImage **m_source, matImage **m_dest, matGrise **mg, noeudHaar **n);


/**
 * @brief Menu chargement
 * @details Permet de choisir une image parmi les images sources et de la charger
 * 
 * @param m_source Adresse du pointeur sur la matImage où l'on charge l'image
 */
void menuChargement(matImage **m_source);

/**
 * @brief Menu des transformations
 * @details La transformation negatif agit sur la matImage m_dest, les autres agissent sur la matGrise mg
 * 
 * @param m_source matImage source
 * @param m_dest matImage dest
 * @param mg matGrise mg
 */
void menuTransformation(matImage *m_source, matImage **m_dest, matGrise **mg);

/**
 * @brief Menu permettant de gérer la compression d'image
 * @details Compression sans perte binaire et niveau de gris
 * 
 * @param m_source Pointeur sur matImage source
 * @param m_dest Pointeur sur matImage dest
 * @param mg Pointeur sur matGrise mg
 */
void menuCompression(matImage *m_source, matImage *m_dest, matGrise *mg);

/**
 * @brief Decompresse une image dans la matImage source
 * @details Decompression sans perte binaire ou niveau de gris
 * 
 * @param m_source Adresse du pointeur sur la matImage source
 * @param m_dest Pointeur sur matImage dest
 * @param mg Pointeur sur matGrise mg
 */
void menuDecompression(matImage **m_source, matImage **m_dest, matGrise **mg);

/**
 * @brief Menu de sauvegarde
 * @details Permet de sauvegarder la matImage source, dest ou la matGrise dans une image BMP 24 bits
 * 
 * @param m_source Pointeur sur matImage source
 * @param m_dest Pointeur sur matImage dest
 * @param mg Pointeur sur matGrise mg
 */
void menuSauvegarde(matImage *m_source, matImage *m_dest, matGrise *mg);

/**
 * @brief Menu permettant d'autres opérations
 * @details Opération possible : Remplacement de la matImage source par la matImage dest ou la matGrise mg
 * 
 * @param m_source Adresse du pointeur sur la matImage source
 * @param m_dest Adresse du pointeur sur la matImage dest
 * @param mg Adresse du pointeur sur la matGrise mg
 */
void menuAutre(matImage **m_source, matImage **m_dest, matGrise **mg);

/**
 * @brief Decompose ou recompose une image à partir d'un arbre de Haar
 * @details Lors de la décomposition, enregistre l'arbre dans le noeudHaar passé par adresse de pointeur, et pour la recomposition, on recompose dans la matrice source et on libere les 2 autres matrices
 * 
 * @param m_source Pointeur sur matImage dest
 * @param m_dest Pointeur sur matImage dest
 * @param mg Pointeur sur matGrise mg
 * @param racine [description]
 */
void menuDecompRecomp(matImage **m_source, matImage **m_dest, matGrise **mg, noeudHaar **racine);

/**
 * @brief Demande le nom de l'image à sauvegarder
 * 
 * @param path chemin
 */
void destinationImage(char *path);

/**
 * @brief Demande le nom du fichier binaire à sauvegarder
 * 
 * @param path chemin
 */
void destinationCompressionBinaire(char *path);

/**
 * @brief Liste les fichiers contenu dans le dossier passé en paramètre
 * @details Source : https://forum.hardware.fr/hfr/Programmation/C/lister-fichiers-repertoire-sujet_68734_1.htm
 * 
 * @param path Chemin vers le dossier
 */
void listeFichiersFrom(char *path);
#endif