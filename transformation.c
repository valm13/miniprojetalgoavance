#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "libISEN/BmpLib.h"
#include "structure.h"
#include "matrice.h"
#include "tri.h"
#include "transformation.h"


void negatifImage(matImage *src, matImage **new)
{
	if(src != NULL)
	{
		if(*new != NULL)
			libere3Matrices(new);
		
		int h = src->nblig;
		int l = src->nbcol;

		alloue3Matrice(src->nblig,src->nbcol,new);
		// Remplissage de la matrice
		for(int i = 0; i < h; i++)
		{
			for (int j = 0; j < l; j++)
			{

				(*new)->r[i][j] = 255 - src->r[i][j];
				(*new)->v[i][j] = 255 - src->v[i][j];
				(*new)->b[i][j] = 255 - src->b[i][j];
			}
		}
	}
}

void couleur2NG(matImage *src, matGrise **new)
{
	if(src != NULL)
	{
		if(*new != NULL)
			libereMatriceGrise(new);
		
		int h = src->nblig;
		int l = src->nbcol;
		int value;

		alloueMatriceGrise(src->nblig,src->nbcol,new);
		// Remplissage de la matrice
		for(int i = 0; i < h; i++)
		{
			for (int j = 0; j < l; j++)
			{
				value = (int)(src->r[i][j] * 0.2125 + src->v[i][j] * 0.7154 + src->b[i][j] * 0.0721);
				(*new)->g[i][j] = value;
			}
		}
	}
}

void seuillageNG(matGrise *src, matGrise **new, int seuil)
{
	if(src != NULL)
	{
		if(*new != NULL)
			libereMatriceGrise(new);
		
		int h = src->nblig;
		int l = src->nbcol;
		int value;

		alloueMatriceGrise(src->nblig,src->nbcol,new);
		// Remplissage de la matrice
		for(int i = 0; i < h; i++)
		{
			for (int j = 0; j < l; j++)
			{
				value = src->g[i][j];
				if(value > seuil)
					(*new)->g[i][j] = 255;
				else
					(*new)->g[i][j] = 0;
			}
		}
	}
}

void filtreMedian(matGrise *src, matGrise **new)
{
	if(src != NULL)
	{
		if(*new != NULL)
			libereMatriceGrise(new);
		
		int h = src->nblig;
		int l = src->nbcol;
		int T[9];
		alloueMatriceGrise(src->nblig,src->nbcol,new);
		// Remplissage de la matrice
		for(int i = 0; i < h; i++)
		{
			for (int j = 0; j < l; j++)
			{
				// On regarde si on est à l'intérieur puis sinon sur un bors et le coin [0][0]

				// Intérieur
				if(i > 0 && i < src->nblig - 1 && j > 0 && j < src->nbcol - 1)
				{
					T[0] = src->g[i][j];
					// Tout autour de la position [i][j]
					T[1] = src->g[i+1][j];
					T[2] = src->g[i-1][j];
					T[3] = src->g[i][j+1];
					T[4] = src->g[i][j-1];
					T[5] = src->g[i+1][j+1];
					T[6] = src->g[i+1][j-1];
					T[7] = src->g[i-1][j+1];
					T[8] = src->g[i-1][j-1];
					// printf("On est à l'intérieur\n");
				}
				// Les différents bords
				// Bord gauche
				else if(j == 0 && i > 0 && i < src->nblig - 1)
				{
					T[0] = src->g[i][j];
					// Les symétriques de la gauche donc à droite car à gauche on sort de l'image
					T[1] = src->g[i+1][j+1];
					T[2] = src->g[i][j+1];
					T[3] = src->g[i-1][j+1];
					// au dessus et en dessous
					T[4] = src->g[i+1][j];
					T[5] = src->g[i-1][j];
					// les 3 de droite
					T[6] = src->g[i+1][j+1];
					T[7] = src->g[i][j+1];
					T[8] = src->g[i-1][j+1];
				}
				// Bord droit	
				else if(j == src->nbcol - 1 && i > 0 && i < src->nblig - 1)
				{
					T[0] = src->g[i][j];
					// Les symétriques de la droite donc à gauche car à droite on sort de l'image
					T[1] = src->g[i+1][j-1];
					T[2] = src->g[i][j-1];
					T[3] = src->g[i-1][j-1];
					// au dessus et en dessous
					T[4] = src->g[i+1][j];
					T[5] = src->g[i-1][j];
					// les 3 de gauche
					T[6] = src->g[i+1][j-1];
					T[7] = src->g[i][j-1];
					T[8] = src->g[i-1][j-1];
				}

				// Bord bas

				else if(i == 0 && j > 0 && j < src->nbcol - 1)
				{
					T[0] = src->g[i][j];
					// Les symétriques du bas donc le haut car en bas on sort de l'image
					T[1] = src->g[i+1][j-1];
					T[2] = src->g[i+1][j];
					T[3] = src->g[i+1][j+1];
					// au droite et à gauche
					T[4] = src->g[i][j-1];
					T[5] = src->g[i][j+1];
					// les 3 d'au dessus
					T[6] = src->g[i+1][j-1];
					T[7] = src->g[i+1][j];
					T[8] = src->g[i+1][j+1];
				}
				// Bord haut
				else if(i == src->nblig - 1 && j > 0 && j < src->nbcol - 1)
				{
					T[0] = src->g[i][j];
					// Les symétriques du haut donc le bas car en haut on sort de l'image
					T[1] = src->g[i-1][j-1];
					T[2] = src->g[i-1][j];
					T[3] = src->g[i-1][j+1];
					// au droite et à gauche
					T[4] = src->g[i][j-1];
					T[5] = src->g[i][j+1];
					// les 3 d'en dessous
					T[6] = src->g[i-1][j-1];
					T[7] = src->g[i-1][j];
					T[8] = src->g[i-1][j+1];
				}
				else if( i == 0 && j == 0)
				{
					T[0] = src->g[i][j];
					// Les 3 seuls réelement existants voisins
					T[1] = src->g[i][j+1];
					T[2] = src->g[i+1][j];
					T[3] = src->g[i+1][j+1];

					T[4] = src->g[i][j+1];
					T[5] = src->g[i+1][j];
					T[6] = src->g[i+1][j+1];

					// Je suppose que le pixel traité est un poil plus important que ceux dupliqués
					// Donc je le triple
					T[7] = src->g[i][j];
					T[8] = src->g[i][j];
				}
				bubbleSortAsc(T,9);
				(*new)->g[i][j] = T[4];
			}
		}
	}
}