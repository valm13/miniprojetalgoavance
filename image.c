#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include "libISEN/BmpLib.h"
#include "structure.h"
#include "image.h"

void sauvegardeImage(DonneesImageRGB *img, char *path)
{
	if(img != NULL)
	{
		if(ecrisBMPRGB_Dans(img,path))
			printf("Image enregistrée\n");
		else
			printf("Image non enregistrée\n");
	}
}

int enleveExtension(char *path)
{
	int nb = 0;
	if(path != NULL)
	{
		nb = strlen(path);
		if(path[nb-1] == 'p' && path[nb-2] == 'm' && path[nb-3] == 'b' && path[nb-4] == '.')
		{
			path[nb-1] = path[nb-2] = path[nb-3] = path[nb-4] =  '\0';
			printf("Extension enlevée\n");
			return 1;
		}
	}
	else
		printf("Pas de chaîne\n");
	return 0;
}

void copieImage(char *path)
{
	DonneesImageRGB *img = lisBMPRGB(path);
	if(img != NULL)
	{	
		printf("Image chargée\n");
		char scdPath[100];
		strcpy(scdPath,path);
		if(enleveExtension(scdPath))
		{
			strcat(scdPath," - Copie.bmp");
			if(ecrisBMPRGB_Dans(img,scdPath))
				printf("Image enregistrée sous %s_1\n",scdPath);
			else
				printf("Impossible d'enregistrer l'image sous %s\n",scdPath);
		}
	}
	else
		printf("Image introuvable\n");
}

void affichePixelsImage(DonneesImageRGB *img)
{
	
	if(img != NULL)
	{
		int l = img->largeurImage;
		int h = img->hauteurImage;
		for(int i = 0; i < h; i++)
			for(int j = 0; j < l; j++)
				printf("Pixel [%d][%d] : B = %d | V = %d | R = %d\n",i, j, img->donneesRGB[i*l+j],img->donneesRGB[i*l+j+1],img->donneesRGB[i*l+j+2]);
	}
	else
		printf("Image NULL\n");
}

void creeImage(int n, int m, short int **r, short int **v, short int **b, DonneesImageRGB **img)
{
	DonneesImageRGB *pic = NULL;
	int nbPixel = m * n;
	int ptr = 0;
	pic = (DonneesImageRGB*)malloc(sizeof(DonneesImageRGB));
	pic->largeurImage = m;
	pic->hauteurImage = n;
	pic->donneesRGB = (unsigned char*)malloc(nbPixel * 3 * sizeof(unsigned char));
	for(int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			/* code */
			pic->donneesRGB[ptr] = (unsigned char)b[i][j];
			ptr++;
			pic->donneesRGB[ptr] = (unsigned char)v[i][j];
			ptr++;
			pic->donneesRGB[ptr] = (unsigned char)r[i][j];
			ptr++;
		}
	}
	*img = pic;
}

void creeImageFromMatImage(matImage *mat, DonneesImageRGB **img)
{
	if(mat != NULL)
	{
		int n = mat->nblig;
		int m = mat->nbcol;
		if( n > 0 && m > 0)
			creeImage(n,m,mat->r,mat->v,mat->b,img);
	}
}

void creeImageFromMatGrise(matGrise *mat, DonneesImageRGB **img)
{
	if(mat != NULL)
	{
		int n = mat->nblig;
		int m = mat->nbcol;
		if( n > 0 && m > 0)
			creeImage(n,m,mat->g,mat->g,mat->g,img);
	}
}